#IQS Morges / seance decembre
#ce script a pour but de calculer les statistques des notes sur des zones donnees
#en comparant les statistques de l'etat actuel avec celles d'un projet on peut voir si le projet ne degrade pas le sol
#l'emprise est definie par des coordonnees, ou alors la base de donnees doit etre decoupee en amont

import sqlite3
from sqlite3 import Error
import shapely
from shapely import wkt
from shapely import wkb
from shapely.wkt import loads
from shapely.geometry import Polygon#, shape, GeometryCollection

#decommenter une des zones et une des databases pour le calcul des notes moyennes !

##Raymondon zone en bas a gauche
#Emin_empr = 2527150
#Nmin_empr = 1151571
#Emax_empr = 2527965
#Nmax_empr = 1152348

##Raymondon zone au centre (zone avec evolution planifiee)
#Emin_empr = 2529870
#Nmin_empr = 1153159
#Emax_empr = 2530283
#Nmax_empr = 1153546

##Raymondon zone en haut a droite
#Emin_empr = 2530682
#Nmin_empr = 1154158
#Emax_empr = 2531502
#Nmax_empr = 1154920

##Magali zone
#Emin_empr = 2524044
#Nmin_empr = 1148458
#Emax_empr = 2524819
#Nmax_empr = 1149217

##Karine zone
#Emin_empr = 2525022
#Nmin_empr = 1150328
#Emax_empr = 2525723
#Nmax_empr = 1150822

bbox = {"Emin_empr" : Emin_empr, "Nmin_empr": Nmin_empr, "Emax_empr": Emax_empr, "Nmax_empr": Nmax_empr}

#database = r"D:\Temp_Bron\08_IQS_Morges\Traitement_donnees\preparation_seance\bd_notee_cas_etude.sqlite"
#database = r"D:\Temp_Bron\08_IQS_Morges\DATA_out\fusion_bd_vectoriel_bat_ndvi_Tout_vpv_buff_not_mod.sqlite"
#database = r"D:\Temp_Bron\08_IQS_Morges\DATA_in\cas_etude_atelier3\Magali_notes_projetees\notes_projetees.sqlite"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")')
    except Error as e:
        print(e)
    return conn

def calcul_note_ponderee(conn, bbox):
    surf_cumul = 0.0
    perm_surf_cumul = 0.0
    perm_surf_fiab_cumul = 0.0
    prof_sol_cumul = 0.0
    prof_sol_fiab_cumul = 0.0
    porosite_cumul = 0.0
    porosite_fiab_cumul = 0.0
    mo_argile_cumul = 0.0
    mo_argile_fiab_cumul = 0.0
    reg_crues_cumul = 0.0
    reg_crues_fiab_cumul = 0.0
    
    Emin_empr = bbox["Emin_empr"]
    Nmin_empr = bbox["Nmin_empr"]
    Emax_empr = bbox["Emax_empr"]
    Nmax_empr = bbox["Nmax_empr"]
    
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    try :
        requ_sel = f"SELECT AsBinary(GEOMETRY) as geom_text, * FROM calc_vectoriel WHERE Intersects (GeomFromText('POLYGON(({Emin_empr} {Nmax_empr}, {Emax_empr} {Nmax_empr}, {Emax_empr} {Nmin_empr}, {Emin_empr} {Nmin_empr}, {Emin_empr} {Nmax_empr}))'), GEOMETRY)=1"
        c.execute(requ_sel)
    except Error as e:
        print(e)
    lst_rows = c.fetchall()
    print(len(lst_rows))
    
    for dic_row in lst_rows:
        #print(dic_row)
        #recuperation des notes de chaque polygone
        perm_surf = float(dic_row["perm_surf"])
        perm_surf_fiab = float(dic_row["perm_surf_fiab"])
        prof_sol = float(dic_row["prof_sol"])
        prof_sol_fiab = float(dic_row["prof_sol_fiab"])
        porosite = float(dic_row["porosite"])
        porosite_fiab = float(dic_row["porosite_fiab"])
        mo_argile = float(dic_row["mo_argile"])
        mo_argile_fiab = float(dic_row["mo_argile_fiab"])
        reg_crues = float(dic_row["reg_crues"])
        reg_crues_fiab = float(dic_row["reg_crues_fiab"])

        wkb_geometry = dic_row["geom_text"]

        obj_geom = shapely.wkb.loads(wkb_geometry)

        xmin_poly = obj_geom.bounds[0]
        ymin_poly = obj_geom.bounds[1]
        xmax_poly = obj_geom.bounds[2]
        ymax_poly = obj_geom.bounds[3]
        
        ##if xmin_poly > Xmin_zone and xmax_poly < Xmax_zone and ymin_poly > Ymin_zone and ymax_poly < Ymax_zone :
        #if xmax_poly > Emin_empr and xmin_poly < Emax_empr and ymax_poly > Nmin_empr and ymin_poly < Nmax_empr : #=> si un polygone touche mon emprise
        surf_poly = obj_geom.area
        surf_cumul = surf_cumul + surf_poly
        perm_surf_cumul = perm_surf_cumul + perm_surf * surf_poly
        perm_surf_fiab_cumul = perm_surf_fiab_cumul + perm_surf_fiab * surf_poly
        prof_sol_cumul = prof_sol_cumul + prof_sol * surf_poly
        prof_sol_fiab_cumul = prof_sol_fiab_cumul + prof_sol_fiab * surf_poly
        porosite_cumul = porosite_cumul + porosite * surf_poly
        porosite_fiab_cumul = porosite_fiab_cumul + porosite_fiab * surf_poly
        mo_argile_cumul = mo_argile_cumul + mo_argile * surf_poly
        mo_argile_fiab_cumul = mo_argile_fiab_cumul + mo_argile_fiab * surf_poly
        reg_crues_cumul = reg_crues_cumul + reg_crues * surf_poly
        reg_crues_fiab_cumul = reg_crues_fiab_cumul + reg_crues_fiab * surf_poly
    
    #surf_moy = surf_cumul / nb_poly
    perm_surf_moy = perm_surf_cumul / surf_cumul
    perm_surf_fiab_moy = perm_surf_fiab_cumul / surf_cumul
    prof_sol_moy = prof_sol_cumul / surf_cumul
    prof_sol_fiab_moy = prof_sol_fiab_cumul / surf_cumul
    porosite_moy = porosite_cumul / surf_cumul
    porosite_fiab_moy = porosite_fiab_cumul / surf_cumul
    mo_argile_moy = mo_argile_cumul / surf_cumul
    mo_argile_fiab_moy = mo_argile_fiab_cumul / surf_cumul
    reg_crues_moy = reg_crues_cumul / surf_cumul
    reg_crues_fiab_moy = reg_crues_fiab_cumul / surf_cumul
    
    notes_moy = {
    "perm_surf_moy" : perm_surf_moy,
    "perm_surf_fiab_moy" : perm_surf_fiab_moy,
    "prof_sol_moy" : prof_sol_moy,
    "prof_sol_fiab_moy" : prof_sol_fiab_moy,
    "porosite_moy": porosite_moy,
    "porosite_fiab_moy" : porosite_fiab_moy,
    "mo_argile_moy" : mo_argile_moy,
    "mo_argile_fiab_moy" : mo_argile_fiab_moy,
    "reg_crues_moy" : reg_crues_moy,
    "reg_crues_fiab_moy" : reg_crues_fiab_moy
    }
    
    return notes_moy
    
conn = create_connection(database)
print(calcul_note_ponderee(conn, bbox))