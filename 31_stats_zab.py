#IQS Morges
#ce script a pour but de calculer les statistques des fiabilites associees aux notes des zones urbaines
#il prend en entree la base de donnees notee et genere un fichier excel contenant les valeurs stats (surf, pourcentage, etc.)

import math
import sqlite3
from sqlite3 import Error
import shapely
from shapely import wkt
from shapely import wkb
from shapely.wkt import loads

database = r"C:\Users\maximin.bron\IQS_MORGES\DATA_out\fusion_bd_vectoriel_bat_ndvi_Tout_vpv_buff_not_mod.sqlite"
file_stat = r"C:\Users\maximin.bron\IQS_MORGES\DATA_out\statistiques.csv"

z_urb = []
z_urb.append("DP")
z_urb.append("Zone d'activités artisanales")
z_urb.append("Zone d'activités tertiaires")
z_urb.append("Zone de camping")
z_urb.append("Zone de centre de localité (zone village)")
z_urb.append("Zone de centre historique")
z_urb.append("Zone de hameau")
z_urb.append("Zone de site construit protégé")
z_urb.append("Zone d'habitation de très faible densité")
z_urb.append("Zone d'habitation de faible densité")
z_urb.append("Zone d'habitation de moyenne densité")
z_urb.append("Zone d'habitation de forte densité")
z_urb.append("Zone de sport et loisirs")
z_urb.append("Zone de verdure")
z_urb.append("Zone d'installations (para-) publiques")
z_urb.append("Zone industrielle")
z_urb.append("Zone à options")
z_urb.append("Zone ferroviaire")
zurb = "(" + str(z_urb)[1:-1] + ")"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")')
    except Error as e:
        print(e)
    return conn

#calcul de la surface totale de la region (10communes)
def calcul_surf_tot(conn):
    surf_tot_cumul = 0.0
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    try :
        requ_sel = f'SELECT AsBinary(GEOMETRY) as geom_bin, * FROM calc_vectoriel'
        c.execute(requ_sel)
    except Error as e:
        print(e)
    lst_rows = c.fetchall()
    for dic_row in lst_rows:
        wkb_geometry = dic_row["geom_bin"]
        obj_geom = shapely.wkb.loads(wkb_geometry)
        surf_poly = obj_geom.area
        surf_tot_cumul = surf_tot_cumul + surf_poly
    print("surf total region ", surf_tot_cumul)
    return surf_tot_cumul

#calcul de la surface des zones urbaines
def calcul_surf_zurb(conn, zurb):
    surf_urb_cumul = 0.0
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    try :
        requ_sel = f'SELECT AsBinary(GEOMETRY) as geom_bin, * FROM calc_vectoriel WHERE paf_type_princ in {zurb}'
        c.execute(requ_sel)
    except Error as e:
        print(e)
    lst_rows_urb = c.fetchall()
    for dic_row in lst_rows_urb:
        wkb_geometry = dic_row["geom_bin"]
        obj_geom = shapely.wkb.loads(wkb_geometry)
        surf_poly = obj_geom.area
        surf_urb_cumul = surf_urb_cumul + surf_poly
    print("surf urb cumul ", surf_urb_cumul)
    return surf_urb_cumul, lst_rows_urb

#calcul fiabilites ponderees
def calcul_stats_fiab_pond(lst_rows_urb):
    surf_1_cumul = 0.0
    surf_2_cumul = 0.0
    surf_3_cumul = 0.0
    surf_4_cumul = 0.0
    surf_5_cumul = 0.0
    surf_6_cumul = 0.0
    perm_fiab_1_cumul = 0.0
    perm_fiab_2_cumul = 0.0
    perm_fiab_3_cumul = 0.0
    perm_fiab_4_cumul = 0.0
    perm_fiab_5_cumul = 0.0
    perm_fiab_6_cumul = 0.0
    ecart_1_cumul = 0.0
    ecart_2_cumul = 0.0
    ecart_3_cumul = 0.0
    ecart_4_cumul = 0.0
    ecart_5_cumul = 0.0
    ecart_6_cumul = 0.0
    
    for dic_row in lst_rows_urb:
        perm_note = float(dic_row["perm_surf"])
        perm_fiab = float(dic_row["perm_surf_fiab"])
        wkb_geometry = dic_row["geom_bin"]
        obj_geom = shapely.wkb.loads(wkb_geometry)
        surf_poly = obj_geom.area
        
        if perm_note <=1 :
            surf_1_cumul = surf_1_cumul + surf_poly
            perm_fiab_1_cumul = perm_fiab_1_cumul + perm_fiab * surf_poly
        elif perm_note > 1 and perm_note <= 2 :
            surf_2_cumul = surf_2_cumul + surf_poly
            perm_fiab_2_cumul = perm_fiab_2_cumul + perm_fiab * surf_poly
        elif perm_note > 2 and perm_note <= 3 :
            surf_3_cumul = surf_3_cumul + surf_poly
            perm_fiab_3_cumul = perm_fiab_3_cumul + perm_fiab * surf_poly
        elif perm_note > 3 and perm_note <= 4 :
            surf_4_cumul = surf_4_cumul + surf_poly
            perm_fiab_4_cumul = perm_fiab_4_cumul + perm_fiab * surf_poly
        elif perm_note > 4 and perm_note <= 5 :
            surf_5_cumul = surf_5_cumul + surf_poly
            perm_fiab_5_cumul = perm_fiab_5_cumul + perm_fiab * surf_poly
        elif perm_note > 5 and perm_note <= 6 :
            surf_6_cumul = surf_6_cumul + surf_poly
            perm_fiab_6_cumul = perm_fiab_6_cumul + perm_fiab * surf_poly
        
    perm_fiab_1_moy = perm_fiab_1_cumul / surf_1_cumul
    perm_fiab_2_moy = perm_fiab_2_cumul / surf_2_cumul
    perm_fiab_3_moy = perm_fiab_3_cumul / surf_3_cumul
    perm_fiab_4_moy = perm_fiab_4_cumul / surf_4_cumul
    perm_fiab_5_moy = perm_fiab_5_cumul / surf_5_cumul
    perm_fiab_6_moy = perm_fiab_6_cumul / surf_6_cumul
    
    #maintenant qu'on a les fiabilites moyennees et ponderees, on peut calculer les ecarts a ces moyennes
    #parcours une seconde fois les polygones pour calculer les ecarts ponderes a la moyenne ponderee
    for dic_row in lst_rows_urb:
        perm_note = float(dic_row["perm_surf"])
        perm_fiab = float(dic_row["perm_surf_fiab"])
        wkb_geometry = dic_row["geom_bin"]
        obj_geom = shapely.wkb.loads(wkb_geometry)
        surf_poly = obj_geom.area
    
        if perm_note <=1 :
            ecart = abs(perm_fiab_1_moy - perm_fiab)
            ecart_1_cumul = ecart_1_cumul + ecart * surf_poly
        elif perm_note > 1 and perm_note <= 2 :
            ecart = (perm_fiab_2_moy - perm_fiab)**2
            ecart_2_cumul = ecart_2_cumul + ecart * surf_poly
        elif perm_note > 2 and perm_note <= 3 :
            ecart = (perm_fiab_3_moy - perm_fiab)**2
            ecart_3_cumul = ecart_3_cumul + ecart * surf_poly
        elif perm_note > 3 and perm_note <= 4 :
            ecart = (perm_fiab_4_moy - perm_fiab)**2
            ecart_4_cumul = ecart_4_cumul + ecart * surf_poly
        elif perm_note > 4 and perm_note <= 5 :
            ecart = (perm_fiab_5_moy - perm_fiab)**2
            ecart_5_cumul = ecart_5_cumul + ecart * surf_poly
        elif perm_note > 5 and perm_note <= 6 :
            ecart = (perm_fiab_6_moy - perm_fiab)**2
            ecart_6_cumul = ecart_6_cumul + ecart * surf_poly
    
    #ecart type = racine de Variance
    std_fiab_1 = math.sqrt(ecart_1_cumul / surf_1_cumul)
    std_fiab_2 = math.sqrt(ecart_2_cumul / surf_2_cumul)
    std_fiab_3 = math.sqrt(ecart_3_cumul / surf_3_cumul)
    std_fiab_4 = math.sqrt(ecart_4_cumul / surf_4_cumul)
    std_fiab_5 = math.sqrt(ecart_5_cumul / surf_5_cumul)
    std_fiab_6 = math.sqrt(ecart_6_cumul / surf_6_cumul)
    
    stats = {
    "surf_1_cumul" : surf_1_cumul, "perm_fiab_1_moy" : perm_fiab_1_moy, "std_fiab_1" : std_fiab_1,
    "surf_2_cumul" : surf_2_cumul, "perm_fiab_2_moy" : perm_fiab_2_moy, "std_fiab_2" : std_fiab_2,
    "surf_3_cumul" : surf_3_cumul, "perm_fiab_3_moy" : perm_fiab_3_moy, "std_fiab_3" : std_fiab_3,
    "surf_4_cumul" : surf_4_cumul, "perm_fiab_4_moy" : perm_fiab_4_moy, "std_fiab_4" : std_fiab_4,
    "surf_5_cumul" : surf_5_cumul, "perm_fiab_5_moy" : perm_fiab_5_moy, "std_fiab_5" : std_fiab_5,
    "surf_6_cumul" : surf_6_cumul, "perm_fiab_6_moy" : perm_fiab_6_moy, "std_fiab_6" : std_fiab_6
    }
    print(stats)
    
    return stats

def ecriture_fichier(surf_tot_cumul, surf_urb_cumul, stats, file_stat):
    with open(file_stat, "w") as f:
        ligne1 = "Statistiques de la perméabilité en zone urbaine"
        ligne2 = f"surface tot région Morges : ; {round((surf_tot_cumul/1000000),1)};[km2]"
        ligne3 = f"surface urbaine : ; {round((surf_urb_cumul/1000000),1)};[km2]"
        ligne4 = f"ratio urb/tot : ;{round((surf_urb_cumul/surf_tot_cumul*100),1)};[%]"
        ligne_vide = ""
        ligne5 = f"note perméabilité;fiabilité moy;écart type de la fiab;surface [km2]; %surf sols urb"
        ligne6 = f"note<=1;{round(stats['perm_fiab_1_moy'],2)};{round(stats['std_fiab_1'],2)};{round(stats['surf_1_cumul']/1000000,2)};{round(stats['surf_1_cumul']/surf_urb_cumul*100,1)}"
        ligne7 = f"1<note<=2;{round(stats['perm_fiab_2_moy'],2)};{round(stats['std_fiab_2'],2)};{round(stats['surf_2_cumul']/1000000,2)};{round(stats['surf_2_cumul']/surf_urb_cumul*100,1)}"
        ligne8 = f"2<note<=3;{round(stats['perm_fiab_3_moy'],2)};{round(stats['std_fiab_3'],2)};{round(stats['surf_3_cumul']/1000000,2)};{round(stats['surf_3_cumul']/surf_urb_cumul*100,1)}"
        ligne9 = f"3<note<=4;{round(stats['perm_fiab_4_moy'],2)};{round(stats['std_fiab_4'],2)};{round(stats['surf_4_cumul']/1000000,2)};{round(stats['surf_4_cumul']/surf_urb_cumul*100,1)}"
        ligne10 = f"4<note<=5;{round(stats['perm_fiab_5_moy'],2)};{round(stats['std_fiab_5'],2)};{round(stats['surf_5_cumul']/1000000,2)};{round(stats['surf_5_cumul']/surf_urb_cumul*100,1)}"
        ligne11 = f"5<note<=6;{round(stats['perm_fiab_6_moy'],2)};{round(stats['std_fiab_6'],2)};{round(stats['surf_6_cumul']/1000000,2)};{round(stats['surf_6_cumul']/surf_urb_cumul*100,1)}"
        
        a_ecrire = f"{ligne1}\n{ligne_vide}\n{ligne2}\n{ligne3}\n{ligne4}\n{ligne_vide}\n{ligne5}\n{ligne6}\n{ligne7}\n{ligne8}\n{ligne9}\n{ligne10}\n{ligne11}"
        f.write(a_ecrire)

conn = create_connection(database)
surf_tot_cumul = calcul_surf_tot(conn)
surf_urb_cumul, lst_rows_urb = calcul_surf_zurb(conn, zurb)
print("ratio urb/tot ", surf_urb_cumul/surf_tot_cumul)
stats = calcul_stats_fiab_pond(lst_rows_urb)
ecriture_fichier(surf_tot_cumul, surf_urb_cumul, stats, file_stat)



