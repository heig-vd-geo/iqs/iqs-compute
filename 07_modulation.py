#ce script prend en entree la BD notee par 06_notation.py
#il module les notes en fonction de criteres et calcule des notes pour des fonctions (ex regulation des crues)
#la bd en sortie a le suffixe _mod.sqlite

import os
import shutil
import sqlite3
from sqlite3 import Error

database_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Tout_vpv_buff_not.sqlite"

def copy_vector_DB(database):
    database_mod = f"{database[:-7]}_mod.sqlite"
    if os.path.exists(database_mod):
        os.remove(database_mod)
        print("old db removed")
    else:
        print("no existing db")
    shutil.copyfile(database, database_mod)
    print("new db created")
    return database_mod

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")')
    except Error as e:
        print(e)
    return conn

def add_column_DB_float(conn, table, nom_col):
    cur   = conn.cursor()
    try:
        addColumn = f"ALTER TABLE {table} ADD COLUMN {nom_col} FLOAT"
        cur.execute(addColumn)
        conn.commit()
    except Error as e:
        print(e)

def modulation_notes_buffer(dic_row):
    #getting existing marks
    perm_surf = float(dic_row["perm_surf"])
    perm_surf_fiab = float(dic_row["perm_surf_fiab"])
    prof_sol = float(dic_row["prof_sol"])
    prof_sol_fiab = float(dic_row["prof_sol_fiab"])
    porosite = float(dic_row["porosite"])
    porosite_fiab = float(dic_row["porosite_fiab"])
    mo_argile = float(dic_row["mo_argile"])
    mo_argile_fiab = float(dic_row["mo_argile_fiab"])
    #buffer batiments souterrains et hors sol
    if dic_row["buffer_bat"] not in [None, "None", ""] or dic_row["buffer_bat_sout"] not in [None, "None", ""]:
        perm_surf = perm_surf * 0.5
        perm_surf_fiab = perm_surf_fiab * 1.2
        prof_sol = prof_sol * 0.5
        prof_sol_fiab = prof_sol_fiab * 1.2
        porosite = porosite * 0.5
        porosite_fiab = porosite_fiab * 1.2
        mo_argile = mo_argile * 0.5
        mo_argile_fiab = mo_argile_fiab * 1.2
    #buffer revetements durs (cff et routes)
    if dic_row["buffer_dur"] not in [None, "None", ""]:
        perm_surf = perm_surf * 0.5
        perm_surf_fiab = perm_surf_fiab * 1.2
        prof_sol = prof_sol * 0.5
        prof_sol_fiab = prof_sol_fiab * 1.2
        porosite = porosite * 0.5
        porosite_fiab = porosite_fiab * 1.2
        mo_argile = mo_argile * 0.5
        mo_argile_fiab = mo_argile_fiab * 1.2
    #else, creation of dic with existing marks from DB
    dic_notes = {"perm_surf" : perm_surf, "perm_surf_fiab" : perm_surf_fiab, "prof_sol" : prof_sol, "prof_sol_fiab" : prof_sol_fiab, "porosite" : porosite, "porosite_fiab" : porosite_fiab, "mo_argile" : mo_argile, "mo_argile_fiab" : mo_argile_fiab }
    return dic_notes

def modulation_notes_sol(dic_row, dic_notes):
    #if it is not a building and if its mark is >1 (excluding water areas, etc. with mark=1), then modification of the marks
    if dic_row["baths_s_design"] in [None, "None", ""] and dic_row["batss_s_design"] in [None, "None", ""] and dic_notes["prof_sol"]>1:
        dic_notes = mod_perm_surf(dic_row, dic_notes)
        dic_notes = mod_prof_sol(dic_row, dic_notes)
        dic_notes = mod_mo_arg(dic_row, dic_notes)
        return dic_notes
    #else, return unchanged marks
    else:
        return dic_notes
            
def mod_perm_surf(dic_row, dic_notes):
    if dic_row["sol_regime_hydr"] == "A hydromorphie faible ou de profondeur":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.9
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Généralement mouillés dès la surface":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.4
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Humides, rarement engorgés jusqu'en surface":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.8
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Modérément engorgés par l'eau":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.9
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Sans excès d'eau":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.3
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Très humide, souvent engorgés jusqu'en surface":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.4
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    
    elif dic_row["sol_texture"] == "Limoneux (l)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.8
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limoneux à limono-argileux (lla)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.7
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limoneux à limono-sableux (lls)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.2
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limoneux à silto-limoneux (lul)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.8
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-argileux (la)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.7
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-argileux à silto-argileux (laua)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.6
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-sableux (ls)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.2
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-sableux à limoneux (lsl)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.1
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-sableux à silto-limoneux (lsul)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.1
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Limono-sableux léger (lS)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.3
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Sableux (s)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.4
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Sablo-limoneux (sl)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.2
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Silto-argileux (ua)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.6
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Silto-limoneux (ul)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 0.7
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Silto-sableux (slu)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1.1
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1.2
    elif dic_row["sol_texture"] == "Tourbe décomposée (H)":
        dic_notes["perm_surf"] = dic_notes["perm_surf"] * 1
        dic_notes["perm_surf_fiab"] = dic_notes["perm_surf_fiab"] * 1
    return dic_notes

def mod_prof_sol(dic_row, dic_notes):
    if dic_row["sol_prof_physio"] == "superficiel (<30 cm)":
        dic_notes["prof_sol"] = 2
        dic_notes["prof_sol_fiab"] = 5
    elif dic_row["sol_prof_physio"] == "assez superficiel (30-50 cm)":
        dic_notes["prof_sol"] = 3
        dic_notes["prof_sol_fiab"] = 5
    elif dic_row["sol_prof_physio"] == "moyennement profond (50-70 cm)":
        dic_notes["prof_sol"] = 4
        dic_notes["prof_sol_fiab"] = 5
    elif dic_row["sol_prof_physio"] == "profond (70-100 cm)":
        dic_notes["prof_sol"] = 5
        dic_notes["prof_sol_fiab"] = 5
    elif dic_row["sol_prof_physio"] == "très profond (>100cm)":
        dic_notes["prof_sol"] = 6
        dic_notes["prof_sol_fiab"] = 5
    
    elif dic_row["sol_pierrosite"] == "Non/peu pierreux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 1
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1
    elif dic_row["sol_pierrosite"] == "Caillouteux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.8
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Blocs":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.7
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Faiblement pierreux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.9
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Graveleux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.9
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Riche en cailloux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.8
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Très caillouteux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.6
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Très graveleux":
        dic_notes["prof_sol"] = dic_notes["prof_sol"] * 0.8
        dic_notes["prof_sol_fiab"] = dic_notes["prof_sol_fiab"] * 1.2
    return dic_notes

def mod_mo_arg(dic_row, dic_notes):
    if dic_row["sol_regime_hydr"] == "A hydromorphie faible ou de profondeur":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.9
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Généralement mouillés dès la surface":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.4
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Humides, rarement engorgés jusqu'en surface":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.8
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Modérément engorgés par l'eau":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.9
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    elif dic_row["sol_regime_hydr"] == "Sans excès d'eau":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 1
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1
    elif dic_row["sol_regime_hydr"] == "Très humide, souvent engorgés jusqu'en surface":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.4
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
     
    elif dic_row["sol_pierrosite"] == "Caillouteux":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.8
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    elif dic_row["sol_pierrosite"] == "Blocs":
        dic_notes["mo_argile"] = dic_notes["mo_argile"] * 0.8
        dic_notes["mo_argile_fiab"] = dic_notes["mo_argile_fiab"] * 1.2
    return dic_notes

def not_fonction_reg_crues(dic_notes):
    dic_notes.update({"reg_crues": 0})
    dic_notes.update({"reg_crues_fiab": 0})
    
    perm = float(dic_notes["perm_surf"])
    poros = float(dic_notes["prof_sol"])
    prof = float(dic_notes["porosite"])
    perm_fiab = float(dic_notes["perm_surf_fiab"])
    poros_fiab = float(dic_notes["prof_sol_fiab"])
    prof_fiab = float(dic_notes["porosite_fiab"])
    
    calc_reg_crues = (6*perm + 2*poros + 2*prof)/10
    calc_reg_crues_fiab = (perm_fiab + poros_fiab + prof_fiab)/3

    #FRC = perm * ((poros*prof/42)+(1/7))
    dic_notes["reg_crues"] = calc_reg_crues
    dic_notes["reg_crues_fiab"] = calc_reg_crues_fiab
    return dic_notes

def set_notes(conn, ogcfid, notes):
    cur   = conn.cursor()
    try:
        sql = '''UPDATE calc_vectoriel SET perm_surf=?, perm_surf_fiab=?, prof_sol=?, prof_sol_fiab=?, porosite=?, porosite_fiab=?, mo_argile=?, mo_argile_fiab=?, reg_crues=?, reg_crues_fiab=? WHERE OGC_FID = ?'''
        values=(
        min(6,max(1,(round(notes["perm_surf"],2)))),
        min(6,max(1,(round(notes["perm_surf_fiab"],2)))),
        min(6,max(1,(round(notes["prof_sol"],2)))),
        min(6,max(1,(round(notes["prof_sol_fiab"],2)))),
        min(6,max(1,(round(notes["porosite"],2)))),
        min(6,max(1,(round(notes["porosite_fiab"],2)))),
        min(6,max(1,(round(notes["mo_argile"],2)))),
        min(6,max(1,(round(notes["mo_argile_fiab"],2)))),
        min(6,max(1,(round(notes["reg_crues"],2)))),
        min(6,max(1,(round(notes["reg_crues_fiab"],2)))),
        ogcfid)
        cur.execute(sql, values)
    except Error as e:
        print(e)

def processing(conn):
    #add new columns in DB
    add_column_DB_float(conn, "calc_vectoriel", "reg_crues")
    add_column_DB_float(conn, "calc_vectoriel", "reg_crues_fiab")
    #getting polygons infos
    #each row is in a dic
    #lst_rows contains all dics
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    requ_sel = "SELECT * FROM calc_vectoriel"
    c.execute(requ_sel)
    lst_rows = c.fetchall()
    #loop to run through each dic (=each row) of the DB
    print("updating DB")
    for dic_row in lst_rows:
        #getting fid to update DB with new marks
        ogcfid = dic_row["OGC_FID"]
        #modifying marks with buffer
        notes_ac_buff = modulation_notes_buffer(dic_row)
        notes_ac_buff_ac_sol = modulation_notes_sol(dic_row, notes_ac_buff)
        #computing marks of function "regulation crues"
        notes = not_fonction_reg_crues(notes_ac_buff_ac_sol)
        #updating DB
        set_notes(conn, ogcfid, notes)
    conn.commit()

database_mod = copy_vector_DB(database_in)
conn = create_connection(database_mod)
processing(conn)
