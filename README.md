# iqs-compute

Ce dépot regroupe l'ensemble des scripts de calcul réalisé dans le cadre du projet IQS-Morges,
réalisé par [HEPIA](https://www.hesge.ch/hepia/), [HEIA-FR](https://www.heia-fr.ch/) et [HEIG-VD](https://heig-vd.ch/) pour [Région-Morges](https://www.regionmorges.ch/).



Les résultats sont visibles à l'adresse suivante : [https://iqs.heig-vd.ch/maps](https://iqs.heig-vd.ch/maps)
