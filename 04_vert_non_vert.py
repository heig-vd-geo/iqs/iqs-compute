# Ce script prend en entrée l'image IR et la base de donnees en sortie du calcul 03_ndvi_batiments.py
# Il sous-decoupe les polygones en fonction du vert/pas vert detecte dans l'image RS
# Le chemin vers OTBcli doit etre renseigne dans _main_
# La BD en sortie a le suffixe vpv.sqlite

from subprocess import call
import sqlite3
from sqlite3 import Error
import json
import shapely
from shapely import wkt
from shapely.geometry import shape, GeometryCollection #Polygon
from shapely.geometry.multipolygon import MultiPolygon
import geodaisy.converters as convert
import gdal
from osgeo.gdalconst import GA_Update
import numpy as np
import os
import shutil

#decommenter UNE DES zones ci dessous

#zone = "Morges_Lonay"
#Xmin_zone = 2527875
#Ymin_zone = 1151810
#Xmax_zone = 2530045
#Ymax_zone = 1153100

#zone = "Lonay_Echandens"
#Xmin_zone = 2529775
#Ymin_zone = 1153065
#Xmax_zone = 2531775
#Ymax_zone = 1154330

#zone = "Morges_Tolochenaz"
#Xmin_zone = 2526155 
#Ymin_zone = 1150325
#Xmax_zone = 2528145
#Ymax_zone = 1151540 

zone = "Tout"
Xmin_zone = 2522200 
Ymin_zone = 1146800
Xmax_zone = 2533000
Ymax_zone = 1158000 


#creation of folder crop_segm_zab in data inter
def create_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    try:
        os.mkdir(folder)
    except OSError:
        print("Creation of the folder %s failed " % folder)

#removing of temp folder
def delete_folder(path_to_folder):
    try :
        shutil.rmtree(path_to_folder)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (path_to_folder, e))
    print("temp folder removed")

        
#creation of a folder with polygons and croped images in DATA_inter\crop_segm_zab\
def creation_folder_zone(path_to_folder, zone):
    path_to_zone = os.path.join(path_to_folder, zone)
    try:
        os.mkdir(path_to_zone)
    except OSError:
        print ("Creation of the directory %s failed" % path_to_zone)
    else:
        print ("Successfully created the directory %s " % path_to_zone)
    #print(path_to_zone)
    return path_to_zone

#delete content of the folder if not empty
def empty_folder(folder_zone):
    for filename in os.listdir(folder_zone):
        file_path = os.path.join(folder_zone, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))

def copy_db(database, zone):
    database_mod = f"{database[:-7]}_{zone}_vpv.sqlite"
    if os.path.exists(database_mod):
        os.remove(database_mod)
    shutil.copyfile(database, database_mod)
    return database_mod

def crop_raster(srcraster, path_to_zone, json, name):
    name_out= f"{name}_crop.tif"
    dstfile = os.path.join(path_to_zone, name_out)
    #certains polygones sont non valides et le cutline ne fonctionne pas...
    crop_raster_with_polygon = f"gdalwarp -s_srs EPSG:2056 -t_srs EPSG:2056 -cutline {json} -crop_to_cutline -overwrite -dstnodata -9999 {srcraster} {dstfile} -wo NUM_THREADS=ALL_CPUS -multi" #-dstnodata 'value[0]'  -srcnodata 'value[-9999]' 
    call(crop_raster_with_polygon)
    return dstfile

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")')
    except Error as e:
        print(e)
    return conn

def add_column_DB_txt(conn, table, nom_col):
    cur = conn.cursor()
    try :
        addColumn = f"ALTER TABLE {table} ADD COLUMN {nom_col} TEXT"
        cur.execute(addColumn)
        conn.commit()
    except Error as e:
        print(e)

def select_in_DB(conn, genre_aff, genre_cs):
    cur = conn.cursor()
    request = f"SELECT astext(geometry), paf_type_princ, baths_s_design, batss_s_design, OGC_FID, csdur_s_genre_txt FROM calc_vectoriel WHERE (paf_type_princ IN {genre_aff} or csdur_s_genre_txt IN {genre_cs})"
    cur.execute(request)
    rows = cur.fetchall()
    return rows
    
####la normalisation n'est normalement pas necessaire ici, elle a deja du etre faite avant...
####def normalisation(image_a_normaliser): #arg : nom de l'image
####    raster_norm = image_a_normaliser[:-4] + "_norm.tif"
####    commande_normalisation = f'C:\\Users\\maximin.bron\\AppData\\Roaming\\QGIS\\QGIS3\\profiles\\default\\python\\plugins\\OTB-7.1.0-Win64\\bin\\otbcli_DynamicConvert.bat -in {image_a_normaliser} -ram=5000 -out {raster_norm} uint16'
####    print(commande_normalisation)
####    call(commande_normalisation)
####    return raster_norm

def compute_ndvi(image, path_OTBcli):
    NDVI = image[:-4] + "_ndvi.tif"
    command_NDVI = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 50000 -il {image} -out {NDVI} -exp "(im1b1-im1b2)/(im1b1+im1b2)"'
    call(command_NDVI)
    return NDVI

#write image (0 = nodata = mask segm / green = 1 / not green = 2)
def green_or_not(ndvi, path_OTBcli):
    img_g_not_g = ndvi[:-4] + "_0.tif"
    command = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 50000 -il {ndvi} -out {img_g_not_g} -exp "(im1b1<=1 and im1b1>=-1) ? ((im1b1<=1 and im1b1>0.2) ? 1 : 2)  : 0"'
    call(command)
    return img_g_not_g

#convert image green/not green to vector polygon
def segmentation(raster, spatialr, ranger, minsize, path_OTBcli):
    raster_out = raster[:-4] + "_seg.tif"
    command_segmentation = f'{path_OTBcli}\otbcli_Segmentation.bat -filter.meanshift.spatialr {spatialr} -filter.meanshift.ranger {ranger} -filter.meanshift.minsize {minsize} -in {raster} -mode raster -mode.raster.out {raster_out} uint16 -progress 0'
    call(command_segmentation)
    return raster_out

#zonal statisics on a raster and export to vector (sqlite)
def get_polygon_stats(g_not_g, img_lab, path_OTBcli):
    input = g_not_g
    polygon_stats = img_lab[:-4] + "_rens.sqlite"
    get_stats = f'{path_OTBcli}\otbcli_ZonalStatistics.bat -inzone labelimage -in {input} -inzone.labelimage.in {img_lab} -out.vector.filename {polygon_stats} -ram 50000 -progress 0'
    call(get_stats)
    return polygon_stats

def compute_g_not_g_update_DB(path_to_zone, rows, Xmin_zone, Ymin_zone, Xmax_zone, Ymax_zone, conn, srcraster, path_OTBcli):#, database_new):
    num = 0
    for row in rows:
        wkt_geometry = row[0]
        zone = row[1]
        building = row[2]
        ug_building = row[3]
        ogcfid = row[4]
        genre_cs = row[5]
        g1 = shapely.wkt.loads(wkt_geometry).buffer(0)
        #filter with polygon size
        if g1.area>5 :
            #test if polygon is in working area
            xmin_poly = g1.bounds[0]
            ymin_poly = g1.bounds[1]
            xmax_poly = g1.bounds[2]
            ymax_poly = g1.bounds[3]
            if xmax_poly > Xmin_zone and xmin_poly < Xmax_zone and ymax_poly > Ymin_zone and ymin_poly < Ymax_zone :
                #conversion en geojson, en AJOUTANT LE CRS !!!
                json = convert.wkt_to_geojson(str(g1))
                json_mn95 = '{"type": "FeatureCollection", "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::2056" } }, "features": [{ "type": "Feature", "geometry":' + json + '}]}'
                nom_json_file = f"{num}_json_file.geojson"
                json_file = os.path.join(path_to_zone, nom_json_file)
                with open(json_file, "w") as js_f:
                    js_f.write(json_mn95)
                
                #getting polygons that are neither building nor underground
                if building in ["None", None, ""] and ug_building in ["None", None, ""]:
                #if (building == None and ug_building == None) or (building == "" and ug_building == "") or (building == "None" and ug_building == "None") : #cest pas un building:    
                    name = str(num)+"_nat"
                    img_cut = crop_raster(srcraster, path_to_zone, json_file, name)
                    #normal = normalisation(img_cut)
                    ndvi = compute_ndvi(img_cut, path_OTBcli)
                    img_g_not_g = green_or_not(ndvi, path_OTBcli)
                    seg_raster = segmentation(img_g_not_g, 0.1,1,100, path_OTBcli)
                    polygon_stats = get_polygon_stats(img_g_not_g, seg_raster, path_OTBcli)

                    dic_attributes = get_attributes_to_keep(conn, ogcfid)

                    try :
                        add_name_to_table(polygon_stats)
                        filter_nodatapolygons(polygon_stats)
                        not_g_raw = get_poly(polygon_stats)
                        poly_shapely = json_to_shapely(json_file)
                        
                        #this step is meant to clean the edges of polygons 
                        poly_green, poly_not_green = difference_poly(poly_shapely, not_g_raw)
                        
                        #write new polygons into database and delete old ones
                        write_into_DB(conn, poly_green, poly_not_green, dic_attributes)
                        delete_from_DB(conn, ogcfid)
                        
                    except Error as e:
                        print(e)

                else : #= if this is a building
                    pass
                num+=1
    conn.commit()

#this function is used to add a name to table with empty name (i.e. when exported from OTB)
def add_name_to_table(sqlite_file):
    conn2 = None
    conn2 = create_connection(sqlite_file)
    conn2.enable_load_extension(True)
    conn2.execute('SELECT load_extension("mod_spatialite")')
    c2 = conn2.cursor()
    c2.execute("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;")
    change_table_name = "ALTER TABLE '' RENAME TO mes_polygones"
    c2.execute(change_table_name)
    change_name_for_geometry = "UPDATE geometry_columns SET f_table_name = 'mes_polygones'"
    c2.execute(change_name_for_geometry)
    conn2.commit()
    conn2.close()
    print("nom ajoute a la table")

def filter_nodatapolygons(sqlite_file):
    conn1 = None
    conn1 = create_connection(sqlite_file)
    c = conn1.cursor()
    sql = "DELETE FROM mes_polygones WHERE mean_0<0.7"
    c.execute(sql)
    conn1.commit()
    conn1.close()
    print("nodata REMOVED")

def get_attributes_to_keep(conn, ogcfid):
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    requ_sel = f"SELECT * FROM calc_vectoriel WHERE OGC_FID = {ogcfid}"
    c.execute(requ_sel)
    lst_rows = c.fetchall()
    dic_attributes = lst_rows[0]#["ed_inventaire_entretien"]
    print("attributs recuperes")
    return dic_attributes
    
def delete_from_DB(conn3, ogcfid):
    cur3 = conn3.cursor()
    requet_suppr = f"DELETE FROM calc_vectoriel WHERE OGC_FID = {ogcfid}"
    cur3.execute(requet_suppr)
    #conn3.commit()
    print("old polygones removed")

#create multipolygon with polygons that have a given value close to 2 (not green)
def get_poly(file): #polygon_stats
    conn_poly = None
    conn_poly = create_connection(file)
    conn_poly.enable_load_extension(True)
    conn_poly.execute('SELECT load_extension("mod_spatialite")')
    cur   = conn_poly.cursor()
    list_poly = []
    try :
        get_pv_poly = f"SELECT AsText(GeomFromWKB(geometry)) from mes_polygones WHERE mean_0 >= 1.7"
        cur.execute(get_pv_poly)
        polygones = cur.fetchall()
        for polygon in polygones :
            p = shapely.wkt.loads(str(polygon[0]))
            list_poly.append(p)
    except Error as e:
        print("error here")
    not_g_raw = MultiPolygon(list_poly)
    return not_g_raw

def json_to_shapely(poly_json):
    with open(poly_json) as f :
        poly_orig = json.load(f)["features"]
        poly_shapely = GeometryCollection([shape(feature["geometry"]).buffer(0) for feature in poly_orig]) #buffer(0) to avoid intersections
    print("polygons imported")
    return poly_shapely

def difference_poly(polyg_origin, not_g_raw):
    green = polyg_origin.difference(not_g_raw.buffer(0))
    try :
        green_cleaned = MultiPolygon([poly for poly in green if poly.area>0.5])#suppression des polygones < 0.5m2
    except TypeError as e:
        print(e)
        green_cleaned = MultiPolygon([green])
    not_green = polyg_origin.difference(green_cleaned)
    return green_cleaned, not_green
    
def write_into_DB(conn_DB, poly_green, poly_not_green, dic_attributes):
    cur = conn_DB.cursor()
    baths_s_design = dic_attributes["baths_s_design"]
    batss_s_design = dic_attributes["batss_s_design"]
    couv_s_design = dic_attributes["couv_s_design"]
    csdur_s_genre_txt = dic_attributes["csdur_s_genre_txt"]
    cseau_s_genre_txt = dic_attributes["cseau_s_genre_txt"]
    dgav_geo_affect = dic_attributes["dgav_geo_affect"]
    ed_inventaire_cat_entret = dic_attributes["ed_inventaire_cat_entret"]
    ed_inventaire_entretien = dic_attributes["ed_inventaire_entretien"]
    paf_type_princ = dic_attributes["paf_type_princ"]
    paf_type_sec = dic_attributes["paf_type_sec"]
    paf_denom_com = dic_attributes["paf_denom_com"]
    sol_pierrosite = dic_attributes["sol_pierrosite"]
    sol_prof_physio = dic_attributes["sol_prof_physio"]
    sol_regime_hydr = dic_attributes["sol_regime_hydr"]
    sol_texture = dic_attributes["sol_texture"]
    #buffer_bat = dic_attributes["buffer_bat"]
    #buffer_bat_sout = dic_attributes["buffer_bat_sout"]
    #buffer_dur = dic_attributes["buffer_dur"]
    
    #check if there is more than one polygon
    try:
        for poly_v in poly_green:
            revetement = "vert"
            commande_ajout_v = f'INSERT INTO calc_vectoriel(GEOMETRY, csdur_s_genre_txt, batss_s_design, ed_inventaire_cat_entret, ed_inventaire_entretien, dgav_geo_affect, paf_type_princ, paf_type_sec, paf_denom_com, couv_s_design, baths_s_design, sol_pierrosite, sol_prof_physio, sol_regime_hydr, sol_texture, revetement) VALUES (GeomFromText("{poly_v}", 2056), "{csdur_s_genre_txt}", "{batss_s_design}", "{ed_inventaire_cat_entret}", "{ed_inventaire_entretien}", "{dgav_geo_affect}", "{paf_type_princ}", "{paf_type_sec}", "{paf_denom_com}", "{couv_s_design}", "{baths_s_design}", "{sol_pierrosite}", "{sol_prof_physio}", "{sol_regime_hydr}", "{sol_texture}", "{revetement}" )'
            cur.execute(commande_ajout_v)
    except TypeError as e:
        print(e)
        revetement = "vert"
        commande_ajout_v = f'INSERT INTO calc_vectoriel(GEOMETRY, csdur_s_genre_txt, batss_s_design, ed_inventaire_cat_entret, ed_inventaire_entretien, dgav_geo_affect, paf_type_princ, paf_type_sec, paf_denom_com, couv_s_design, baths_s_design, sol_pierrosite, sol_prof_physio, sol_regime_hydr, sol_texture, revetement) VALUES (GeomFromText("{poly_green}", 2056),  "{csdur_s_genre_txt}", "{batss_s_design}", "{ed_inventaire_cat_entret}", "{ed_inventaire_entretien}", "{dgav_geo_affect}", "{paf_type_princ}", "{paf_type_sec}", "{paf_denom_com}", "{couv_s_design}", "{baths_s_design}", "{sol_pierrosite}", "{sol_prof_physio}", "{sol_regime_hydr}", "{sol_texture}", "{revetement}" )'
        cur.execute(commande_ajout_v)
    try :
        for poly_pv in poly_not_green:
            revetement = "pas_vert"
            commande_ajout_pv = f'INSERT INTO calc_vectoriel(GEOMETRY, csdur_s_genre_txt, batss_s_design, ed_inventaire_cat_entret, ed_inventaire_entretien, dgav_geo_affect, paf_type_princ, paf_type_sec, paf_denom_com, couv_s_design, baths_s_design, sol_pierrosite, sol_prof_physio, sol_regime_hydr, sol_texture, revetement) VALUES (GeomFromText("{poly_pv}", 2056), "{csdur_s_genre_txt}", "{batss_s_design}", "{ed_inventaire_cat_entret}", "{ed_inventaire_entretien}", "{dgav_geo_affect}", "{paf_type_princ}", "{paf_type_sec}", "{paf_denom_com}", "{couv_s_design}", "{baths_s_design}", "{sol_pierrosite}", "{sol_prof_physio}", "{sol_regime_hydr}", "{sol_texture}", "{revetement}" )'
            cur.execute(commande_ajout_pv)
            #conn_DB.commit()
    except TypeError as e:
        print(e)
        revetement = "pas_vert"
        commande_ajout_pv = f'INSERT INTO calc_vectoriel(GEOMETRY, csdur_s_genre_txt, batss_s_design, ed_inventaire_cat_entret, ed_inventaire_entretien, dgav_geo_affect, paf_type_princ, paf_type_sec, paf_denom_com, couv_s_design, baths_s_design, sol_pierrosite, sol_prof_physio, sol_regime_hydr, sol_texture, revetement) VALUES (GeomFromText("{poly_not_green}", 2056),  "{csdur_s_genre_txt}", "{batss_s_design}", "{ed_inventaire_cat_entret}", "{ed_inventaire_entretien}", "{dgav_geo_affect}", "{paf_type_princ}", "{paf_type_sec}", "{paf_denom_com}", "{couv_s_design}", "{baths_s_design}", "{sol_pierrosite}", "{sol_prof_physio}", "{sol_regime_hydr}", "{sol_texture}", "{revetement}" )'
        cur.execute(commande_ajout_pv)
    #conn_DB.commit()


#fonctions pour renseigner la hauteur de vegetation par polygone. 
#Non fonctionnel pour le moment... A MODIFIER SI ON CONTINUE SUR LE PROJET
def decoupe_MNH_zones_vertes(conne, path_to_zone, bd_polygones_v_pas_v):
    MNH = r".\..\DATA_inter\MNH.tif"
    #set nodata value for input raster (sinon ca utilise la valeur 0 comme nodata et les stats sont fausses)
    filename = MNH
    print(filename)
    nodata = -9999
    ras = gdal.Open(filename, GA_Update)
    #loop through the image bands
    #for i in range(1, ras.RasterCount + 1):
    #    # set the nodata value of the band
    ras.GetRasterBand(1).SetNoDataValue(nodata)
    # unlink the file object and save the results
    ras = None
    #recuperation des polygones "verts"
    cur = conne.cursor()
    cur.execute("SELECT OGC_FID, AsText(GeomFromWKB(GEOMETRY)) FROM mes_polygones WHERE mean_0 < 1.5")
    polygones = (cur.fetchall())
    numero=0
    for polygone_v_pas_v in polygones :
        num_poly = polygone_v_pas_v[0]
        wkt_geometry= polygone_v_pas_v[1]
        obj_geom = shapely.wkt.loads(wkt_geometry)
        #conversion en geojson, en AJOUTANT LE CRS !!
        json = convert.wkt_to_geojson(str(obj_geom))
        json_mn95= '{"type": "FeatureCollection", "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::2056" } }, "features": [{ "type": "Feature", "geometry":' + json + '}]}'
        #print(json_mn95)
        json_file = os.path.join(path_to_zone, "json_file_MNH.geojson")
        with open(json_file, "w") as js_f:
            js_f.write(json_mn95)
        nom = str(numero)+"_arb"
        poly_decoup = crop_raster(MNH, path_to_zone, json_file, nom)
        numero+=1
    print(num_poly)

def remplis_les_hauteur_veg(conn, poly_decoup, num_poly):
    raster = gdal.Open(poly_decoup)
    print("poly_decoup : ", poly_decoup)
    data = raster.GetRasterBand(1).ReadAsArray().astype('float')
    meandata = np.mean(data[data>=0])#exclu les nodata (= -9999)
    maxdata = np.max(data[data>=0])
    #ajout de la valeur moyenne et max dans la DB
    sql_moy = '''UPDATE mes_polygones SET hmoy_veg=? WHERE OGC_FID = ?'''
    sql_max = '''UPDATE mes_polygones SET hmax_veg=? WHERE OGC_FID = ?'''
    cur = conn.cursor()
    values_moy=(round(meandata,5), num_poly)
    values_max=(round(maxdata,5), num_poly)
    cur.execute(sql_moy, values_moy)
    cur.execute(sql_max, values_max)
    conn.commit()

def main():
    
    #path to OTB
    path_OTBcli = r".\..\SOFTS\OTB-7.1.0-Win64\bin"
    
    #folder that will contain temp results...
    path_to_folder = r".\..\DATA_inter"
    
    #input vector DB
    database = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi.sqlite"

    #input raster (SwissImageRS)
    srcraster_RS = r".\..\DATA_out\mosaic_RS_ui16_norm.tif"
    
    #Copy of the DB prior to edition
    database_mod = copy_db(database, zone)
        
    #creation of temp folder
    create_folder(path_to_folder)
    
    #creation of subfolder for each "working zone"
    path_to_zone = creation_folder_zone(path_to_folder, zone)
    
    #delete existing results in working folder
    empty_folder(path_to_zone)
    
    #choix des zones d'affectation a traiter pour le calcul du vert / pas vert
    zones_aff = []
    zones_aff.append("Zone d'activités artisanales")
    zones_aff.append("Zone d'activités tertiaires")
    zones_aff.append("Zone de camping")
    zones_aff.append("Zone de centre de localité (zone village)")
    zones_aff.append("Zone de centre historique")
    zones_aff.append("Zone de hameau")
    zones_aff.append("Zone de site construit protégé")
    zones_aff.append("Zone d'habitation de très faible densité")
    zones_aff.append("Zone d'habitation de faible densité")
    zones_aff.append("Zone d'habitation de moyenne densité")
    zones_aff.append("Zone d'habitation de forte densité")
    zones_aff.append("Zone de sport et loisirs")
    zones_aff.append("Zone de verdure")
    zones_aff.append("Zone d'installations (para-) publiques")
    zones_aff.append("Zone industrielle")
    zones_aff.append("Zone à options")
    
    genre_aff = "(" + str(zones_aff)[1:-1] + ")"
    
    zones_cs = ["chemin de fer"]
    genre_cs = "(" + str(zones_cs)[1:-1] + ")"
    
    #table containing those "zones d'affectation"
    table = "calc_vectoriel"
    
    #name of the new column that will contain the ground covering
    name_col_revetement = "revetement"

    #connection to DB to edit
    conn = create_connection(database_mod)
    with conn:
        add_column_DB_txt(conn, table, name_col_revetement)
        rows = select_in_DB(conn, genre_aff, genre_cs)

        compute_g_not_g_update_DB(path_to_zone, rows, Xmin_zone, Ymin_zone, Xmax_zone, Ymax_zone, conn, srcraster_RS, path_OTBcli)#, database_new)

    #removing temp folder
    delete_folder(path_to_folder)
    
if __name__ == '__main__':
    main()