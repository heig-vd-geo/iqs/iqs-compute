#ce script prend en entree un fichier vectoriel (ici le fichier sqlite contenant les notes des polygones)
#il prend egalement en entree un fichier de coloration de la carte
#il genere des tuiles raster colorisee et des fichiers html pour un affichage dans un navigateur

import os
import shutil
import subprocess

data_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Tout_vpv_buff_not_mod.sqlite"
color_file = r".\cm.txt"
folder_out = r".\..\DATA_out\visualisation"

#creation of folder visualisation in data_out
def create_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    else:
        pass
    try:
        os.mkdir(folder)
    except OSError:
        print("Creation of the folder %s failed " % folder)

def build_webview(data_in, color_file, folder_out):
    for p in ["perm_surf", "perm_surf_fiab", "prof_sol", "prof_sol_fiab", "porosite", "porosite_fiab", "mo_argile", "mo_argile_fiab", "reg_crues", "reg_crues_fiab"]:
        cmd_rast = f'gdal_rasterize -a {p} -tr 0.5 0.5 -a_nodata 0 "{data_in}" "{folder_out}\\note_{p}.tif"'
        subprocess.call(cmd_rast)
        cmd_DEM = f'gdaldem "color-relief" "{folder_out}\\note_{p}.tif" "{color_file}" "{folder_out}\\note_color_{p}.tif"'
        subprocess.call(cmd_DEM)
        cmd_tiles = f'gdal2tiles -z 11-18 {folder_out}\\note_color_{p}.tif {folder_out}\\note_color_{p}'
        os.system(cmd_tiles)
        os.remove(f"{folder_out}\\note_{p}.tif")
        os.remove(f"{folder_out}\\note_color_{p}.tif")

create_folder(folder_out)
build_webview(data_in, color_file, folder_out)