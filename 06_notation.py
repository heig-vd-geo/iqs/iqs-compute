#ce script prend en entree la BD issue du calcul FME 05_ajout_tampons.fmw
#il attribue des notes aux polygones, en fonction de leurs attributs, à chaque niveau du logigramme
#dans le futur, il faudrait remplacer toutes les notes ecrites ici en dur par la lecture d'une base de donnees des notes...
#la bd en sorie a le suffixe _not.sqlite

import os
import shutil
import sqlite3
from sqlite3 import Error

#database_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Lonay_Echandens_vpv_buff.sqlite"
#database_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Morges_Lonay_vpv_buff.sqlite"
#database_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Morges_Tolochenaz_vpv_buff.sqlite"
database_in = r".\..\DATA_out\fusion_bd_vectoriel_bat_ndvi_Tout_vpv_buff.sqlite"
temp_folder = r".\..\DATA_inter"


#creation of folder crop_segm_zab in data inter
def create_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    try:
        os.mkdir(folder)
    except OSError:
        print("Creation of the folder %s failed " % folder)

#removing of temp folder
def delete_folder(path_to_folder):
    try :
        shutil.rmtree(path_to_folder)
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (path_to_folder, e))
    print("temp folder removed")

def copy_vector_DB(database):
    database_not = f"{database[:-7]}_not.sqlite"
    if os.path.exists(database_not):
        os.remove(database_not)
        print("old db removed")
    else:
        print("no existing db")
    shutil.copyfile(database, database_not)
    print("new db created")
    return database_not

def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")')
    except Error as e:
        print(e)
    return conn

def add_column_DB_float(conn, table, name_col):
    cur   = conn.cursor()
    try:
        addColumn = f"ALTER TABLE {table} ADD COLUMN {name_col} FLOAT"
        cur.execute(addColumn)
        conn.commit()
    except Error as e:
        print(e)

def set_notes(conn, ogcfid, notes):
    cur   = conn.cursor()
    try:
        sql = '''UPDATE calc_vectoriel SET perm_surf=?, perm_surf_fiab=?, prof_sol=?, prof_sol_fiab=?, porosite=?, porosite_fiab=?, mo_argile=?, mo_argile_fiab=? WHERE OGC_FID = ?'''
        print(notes)
        values=(notes["perm_surf"], notes["perm_surf_fiab"], notes["prof_sol"], notes["prof_sol_fiab"], notes["porosite"], notes["porosite_fiab"], notes["mo_argile"], notes["mo_argile_fiab"], ogcfid)
        cur.execute(sql, values)
        #conn.commit()
    except Error as e:
        print(e)

def removing_zeros(conn):
    cur   = conn.cursor()
    try:
        sql = '''DELETE FROM calc_vectoriel WHERE prof_sol=0'''
        cur.execute(sql)
    except Error as e:
        print(e)

def processing(conn):
    #adding new columns in table
    add_column_DB_float(conn, "calc_vectoriel", "perm_surf")
    add_column_DB_float(conn, "calc_vectoriel", "perm_surf_fiab")
    add_column_DB_float(conn, "calc_vectoriel", "prof_sol")
    add_column_DB_float(conn, "calc_vectoriel", "prof_sol_fiab")
    add_column_DB_float(conn, "calc_vectoriel", "porosite")
    add_column_DB_float(conn, "calc_vectoriel", "porosite_fiab")
    add_column_DB_float(conn, "calc_vectoriel", "mo_argile")
    add_column_DB_float(conn, "calc_vectoriel", "mo_argile_fiab")
    
    #getting polygons infos
    #each row is in a dic
    #lst_rows contains all dics
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    requ_sel = "SELECT * FROM calc_vectoriel"
    c.execute(requ_sel)
    lst_rows = c.fetchall()
    #loop to run through each dic (=each row) of the DB
    for dic_row in lst_rows:
        #creation of a dic which will be modified along the script, at each level of the logigram
        dic_notes = {"perm_surf" : -1, "perm_surf_fiab" : -1, "prof_sol" : -1, "prof_sol_fiab" : -1, "porosite" : -1, "porosite_fiab" : -1, "mo_argile" : -1, "mo_argile_fiab" : -1}
        #getting fid to update DB with new marks
        ogcfid = dic_row["OGC_FID"]
        #computing marks
        notes = not_niv_0_bat_dur_nat(dic_row, dic_notes)
        #updating DB
        set_notes(conn, ogcfid, notes)
    conn.commit()
    #removing polygons with mark = 0 (unvalid polygons)
    removing_zeros(conn)
    conn.commit()

def not_niv_0_bat_dur_nat(dic_row, dic_notes):
    if dic_row["baths_s_design"] not in ["None", None, ""] or dic_row["batss_s_design"] not in ["None", None, ""]:
        #c'est un batiment
        return not_niv_1_bat(dic_row, dic_notes)
    elif dic_row["csdur_s_genre_txt"] not in ["None", None, ""] or dic_row["cseau_s_genre_txt"] not in ["None", None, ""] or dic_row["paf_type_princ"] == "Zone ferroviaire": #["accès, place privée", "bassin", "chemin de fer", "îlot", "route, chemin", "trottoir, place piétonnière"]:
        #s'ils ont des infos de genre dur ou eau, alors c'est des revetements durs cadastres ou de la flotte
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 1, "prof_sol_fiab" : 6, "porosite" : 1, "porosite_fiab" : 6, "mo_argile" : 1, "mo_argile_fiab" : 6 }
        return not_niv_1_dur(dic_row, dic_notes)
    elif dic_row["dgav_geo_affect"] not in ["None", None, ""]:
        dic_notes = {"perm_surf" : 16, "perm_surf_fiab" : 16, "prof_sol" : 16, "prof_sol_fiab" : 16, "porosite" : 16, "porosite_fiab" : 16, "mo_argile" : 16, "mo_argile_fiab" : 16 }
        return not_niv_1_sar(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] not in ["None", None, ""]:
        #dic_notes = {"perm_surf" : 20, "perm_surf_fiab" : 20, "prof_sol" : 20, "prof_sol_fiab" : 20, "porosite" : 20, "porosite_fiab" : 20, "mo_argile" : 20, "mo_argile_fiab" : 20 }
        return not_niv_1_ED(dic_row, dic_notes)  
    elif dic_row["paf_type_princ"] not in ["None", None, ""]:
        #c'est pas un batiment et il a une affectation
        return not_niv_1_aff(dic_row, dic_notes)
    else:
        #POUR l'instant, il devrait rester uniquement les zones qui sont nulle part (restes d'imprecisions de decoupages ...)
        dic_notes = {"perm_surf" : 0, "perm_surf_fiab" : 0, "prof_sol" : 0, "prof_sol_fiab" : 0, "porosite" : 0, "porosite_fiab" : 0, "mo_argile" : 0, "mo_argile_fiab" : 0 }
        return dic_notes

def not_niv_1_dur(dic_row, dic_notes):
    if dic_row["csdur_s_genre_txt"]  == "chemin de fer" or dic_row["paf_type_princ"] == "Zone ferroviaire":
        dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 2.5, "prof_sol_fiab" : 1, "porosite" : 2.5, "porosite_fiab" : 1, "mo_argile" : 2.5, "mo_argile_fiab" : 1 }
        return not_niv_2_dur_cff(dic_row, dic_notes)
    else:
        return dic_notes

def not_niv_2_dur_cff(dic_row, dic_notes):
    ####NOTATION des chemins de fer en attribuant une meilleure note si revetement est vert et moins bonne sinon
    if dic_row["revetement"] == "vert":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["revetement"] == "pas_vert":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 2, "prof_sol" : 2, "prof_sol_fiab" : 2, "porosite" : 2, "porosite_fiab" : 2, "mo_argile" : 2, "mo_argile_fiab" : 2 }
        return dic_notes
    else:
        return dic_notes

def not_niv_1_bat(dic_row, dic_notes):
    #### NOTATION des batiments hors sol
    if dic_row["baths_s_design"] not in ["None", None, ""]:
        dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 1.5, "prof_sol_fiab" : 1, "porosite" : 2.5, "porosite_fiab" : 1, "mo_argile" : 1.5, "mo_argile_fiab" : 1 }
        return not_niv_2_bat_hs(dic_row, dic_notes)
    #### NOTATION des batiments souterrains
    elif dic_row["batss_s_design"] not in ["None", None, ""]:
        dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 2.5, "prof_sol_fiab" : 1, "porosite" : 2.5, "porosite_fiab" : 1, "mo_argile" : 2.5, "mo_argile_fiab" : 1 }
        return not_niv_2_bat_ss(dic_row, dic_notes)
    else :
        return dic_notes   

def not_niv_2_bat_hs(dic_row, dic_notes):
    #certains batiments avec des geometries non valides n'ont pas de ndvi...
    if dic_row["ndvi"] in ["None", None, ""]:
        return dic_notes
    elif dic_row["ndvi"] > 0.15:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 5, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 2, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["ndvi"] <= 0.15:
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 1, "prof_sol_fiab" : 6, "porosite" : 1, "porosite_fiab" : 6, "mo_argile" : 1, "mo_argile_fiab" : 6 }
        return dic_notes
    else:
        #s'il y a des 14 en sortie, alors il y a un souci avant ca...
        dic_notes = {"perm_surf" : 14, "perm_surf_fiab" : 14, "prof_sol" : 14, "prof_sol_fiab" : 14, "porosite" : 14, "porosite_fiab" : 14, "mo_argile" : 14, "mo_argile_fiab" : 14 }
        return dic_notes
        
def not_niv_2_bat_ss(dic_row, dic_notes):
    #certains batiments avec des geometries non valides n'ont pas de ndvi...
    if dic_row["ndvi"] in ["None", None, ""]:
        return dic_notes
    elif dic_row["ndvi"] > 0.15:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["ndvi"] <= 0.15:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 2, "prof_sol" : 2, "prof_sol_fiab" : 2, "porosite" : 2, "porosite_fiab" : 2, "mo_argile" : 2, "mo_argile_fiab" : 2 }
        return dic_notes
    else:
        #s'il y a des 15 en sortie, alors il y a un souci avant ca...
        dic_notes = {"perm_surf" : 15, "perm_surf_fiab" : 15, "prof_sol" : 15, "prof_sol_fiab" : 15, "porosite" : 15, "porosite_fiab" : 15, "mo_argile" : 15, "mo_argile_fiab" : 15 }
        return dic_notes

def not_niv_1_sar(dic_row, dic_notes):
    lst_prairies_paturages = [611, 612, 613, 616, 617, 618, 625, 634, 650, 660, 693, 694, 697, 698]
    lst_gr_cultures = [501, 502, 504, 505, 506, 507, 508, 509, 511, 512, 513, 514, 515, 516, 519, 521, 522, 523, 524, 525, 526, 527, 528, 531, 534, 535, 536, 537, 538, 539, 541, 542, 543, 544, 548, 549, 550, 552, 555, 556, 557, 559, 566, 567, 568, 569, 572, 573, 574, 590, 591, 592, 594, 595, 597, 598, 601, 602, 631, 632]
    #lst_surf_estivage = []
    lst_cult_speciale = [545, 546, 547, 551, 553, 554, 721, 801, 803, 808, 810, 830, 840, 847, 848, 849, 802, 806, 807]
    lst_vignoble = [701, 717, 722, 735]
    lst_cult_fruitiere = [702, 703, 704, 730, 731]
    lst_autr_cult_per = [705, 706, 707, 709, 710, 711, 712, 713, 714, 715, 718, 719, 720, 725, 750, 760, 797, 798]
    #lst_surf_litiere = []
    lst_autre_SAU = [897, 898]
    lst_haies_bosquets = [852, 857, 858]
    lst_foret = [901]
    lst_surf_hors_SAU = [902, 903, 904, 905, 906, 907, 908, 909, 911, 921, 922, 923, 942, 927, 928, 930, 933, 935, 936, 998]
    autres = [851, 621, 622, 623]
    
    if int(dic_row["dgav_geo_affect"][:3]) in lst_prairies_paturages:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_prai_patu(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_gr_cultures:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_gr_cult(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_cult_speciale:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_cult_speciale(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_vignoble:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_vignoble(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_cult_fruitiere:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 5, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_autr_cult_per:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_autr_cult_per(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_autre_SAU :
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 1, "prof_sol" : 3, "prof_sol_fiab" : 1, "porosite" : 3, "porosite_fiab" : 1, "mo_argile" : 3, "mo_argile_fiab" : 1 }
        return dic_notes
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_haies_bosquets:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return not_niv_2_sar_haies_bosquets(dic_row, dic_notes)
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_foret:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 6, "prof_sol_fiab" : 4, "porosite" : 6, "porosite_fiab" : 5, "mo_argile" : 6, "mo_argile_fiab" : 5 }
        return dic_notes
    elif int(dic_row["dgav_geo_affect"][:3]) in lst_surf_hors_SAU:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return not_niv_2_sar_surf_hors_SAU(dic_row, dic_notes)
    else: #il ne doit pas y en avoir...
        dic_notes = {"perm_surf" : 15, "perm_surf_fiab" : 15, "prof_sol" : 15, "prof_sol_fiab" : 15, "porosite" : 15, "porosite_fiab" : 15, "mo_argile" : 15, "mo_argile_fiab" : 15 }
        return dic_notes
    
def not_niv_2_sar_prai_patu(dic_row, dic_notes):
    #paturages permanents
    if int(dic_row["dgav_geo_affect"][:3]) in [616, 617, 618, 625, 660, 693]:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_paturage_perm(dic_row, dic_notes)
    #prairies permanentes
    elif int(dic_row["dgav_geo_affect"][:3]) in [611, 612, 613, 634, 650, 694, 697, 698]:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_prairie_perm(dic_row, dic_notes)
    else:
        return dic_notes
                
def not_niv_2_sar_gr_cult(dic_row, dic_notes):
    #prairie temporaire
    if int(dic_row["dgav_geo_affect"][:3]) in [601, 602]:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    #SPB
    elif int(dic_row["dgav_geo_affect"][:3]) in [555, 556, 557, 559, 572, 594, 597]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return not_niv_3_sar_gr_cult_SPB(dic_row, dic_notes)
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [501, 502, 504, 505, 506, 507, 508, 509, 511, 512, 513, 514, 515, 516, 519, 521, 522, 523, 524, 525, 526, 527, 528, 531, 534, 535, 536, 537, 538, 539, 541, 542, 543, 544, 548, 549, 550, 552, 566, 567, 568, 569, 595, 598, 631, 632, 573, 574, 590, 591, 592]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_gr_cult_autre(dic_row, dic_notes)
    else:
        return dic_notes

def not_niv_2_sar_cult_speciale(dic_row, dic_notes):
    #maraichage
    if int(dic_row["dgav_geo_affect"][:3]) in [545, 546, 547, 551, 553, 801, 806]:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_cult_spec_maraich(dic_row, dic_notes)
    #horticulture
    elif int(dic_row["dgav_geo_affect"][:3]) in [554, 721, 803, 808]:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_cult_spec_horti(dic_row, dic_notes)
    #autre sous abris
    elif int(dic_row["dgav_geo_affect"][:3]) in [802, 807, 810, 830, 840, 847, 848, 849]:
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes
    
def not_niv_2_sar_vignoble(dic_row, dic_notes):
    #avec biodiv nat
    if int(dic_row["dgav_geo_affect"][:3]) == 717:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 5, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    #en SPB
    elif int(dic_row["dgav_geo_affect"][:3]) == 735:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 5, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [701, 722]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes
    
def not_niv_2_sar_autr_cult_per(dic_row, dic_notes):
    #permaculture
    if int(dic_row["dgav_geo_affect"][:3]) == 725:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    #plein champ
    elif int(dic_row["dgav_geo_affect"][:3]) in [705, 706, 707, 709, 710, 711, 712, 713, 714, 715, 718, 719, 720, 760]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_3_sar_autr_cult_per_plein_champ(dic_row, dic_notes)
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [750, 797, 798]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes
  
def not_niv_2_sar_haies_bosquets(dic_row, dic_notes):
    #SPB
    if int(dic_row["dgav_geo_affect"][:3]) == 858:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [852, 857]:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes

def not_niv_2_sar_surf_hors_SAU(dic_row, dic_notes):
    #SPB
    if int(dic_row["dgav_geo_affect"][:3]) == 928:
        '''pas dans le tableau'''
        dic_notes = {"perm_surf" : 26, "perm_surf_fiab" : 26, "prof_sol" : 26, "prof_sol_fiab" : 26, "porosite" : 26, "porosite_fiab" : 26, "mo_argile" : 26, "mo_argile_fiab" : 26 }
        return dic_notes
    #chemin naturel
    elif int(dic_row["dgav_geo_affect"][:3]) == 907:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 6, "prof_sol" : 1, "prof_sol_fiab" : 6, "porosite" : 1, "porosite_fiab" : 6, "mo_argile" : 1, "mo_argile_fiab" : 6 }
        return dic_notes
    #jardin potager
    elif int(dic_row["dgav_geo_affect"][:3]) == 909:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    #surface improductive
    elif int(dic_row["dgav_geo_affect"][:3]) == 902:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 3 }
        return dic_notes
    #fosses, mares, etangs...
    elif int(dic_row["dgav_geo_affect"][:3]) == 904:
        '''pas dans le logi'''
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 1, "prof_sol_fiab" : 6, "porosite" : 1, "porosite_fiab" : 6, "mo_argile" : 1, "mo_argile_fiab" : 6 }
        return dic_notes
    #autres
    elif int(dic_row["dgav_geo_affect"][:3]) in [903, 905, 906, 908, 911, 921, 922, 923, 942, 927, 930, 933, 935, 936, 998]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_sar_paturage_perm(dic_row, dic_notes):
    #paturage extensif
    if int(dic_row["dgav_geo_affect"][:3]) == 617:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 5, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [616, 618, 625, 660, 693]:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes
    
def not_niv_3_sar_prairie_perm(dic_row, dic_notes):
    #extensive
    if int(dic_row["dgav_geo_affect"][:3]) == 611:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 5, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 5, "mo_argile" : 5, "mo_argile_fiab" : 5 }
        return dic_notes
    #peu intensive
    elif int(dic_row["dgav_geo_affect"][:3]) == 612:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 5, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 5, "mo_argile" : 5, "mo_argile_fiab" : 5 }
        return dic_notes
    #autre
    if int(dic_row["dgav_geo_affect"][:3]) in [613, 634, 650, 694, 697, 698]:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_sar_gr_cult_SPB(dic_row, dic_notes):
    #prairie temporaire jachere florale
    if int(dic_row["dgav_geo_affect"][:3]) == 556:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    #prairie temporaire jachere tournante
    if int(dic_row["dgav_geo_affect"][:3]) == 557:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes    
    #bande fleurie
    elif int(dic_row["dgav_geo_affect"][:3]) == 572:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    #autre
    elif int(dic_row["dgav_geo_affect"][:3]) in [555, 559, 594, 597]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_sar_gr_cult_autre(dic_row, dic_notes):
    #cultures sarclees
    if int(dic_row["dgav_geo_affect"][:3]) in [508, 521, 522, 524]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    #meteil legumineuse
    elif int(dic_row["dgav_geo_affect"][:3]) in [569]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_sar_cult_spec_maraich(dic_row, dic_notes):
    #abris
    if int(dic_row["dgav_geo_affect"][:3]) in [801, 806]:
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    #plein champ
    elif int(dic_row["dgav_geo_affect"][:3]) in [545, 546, 547, 551, 553]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    #autre (present dans le logigramme mais pas dans le tableau)
    else:
        return dic_notes

def not_niv_3_sar_cult_spec_horti(dic_row, dic_notes):
    #sous abris
    if int(dic_row["dgav_geo_affect"][:3]) in [803, 808]:
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    #autre (pas present dans le logigramme)
    else:
        return dic_notes

def not_niv_3_sar_autr_cult_per_plein_champ(dic_row, dic_notes):
    #baies plurianuelles plantes aromatiques plurianuelles mpr plurianuelles
    if int(dic_row["dgav_geo_affect"][:3]) in [705, 706, 707, 709, 710]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    #autres pepinieres, chataigneraies, sapins de Noel, truffieres
    elif int(dic_row["dgav_geo_affect"][:3]) in [715, 720, 712, 718]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes

#def not_niv_4_sar_gr_cult_SPB_jach(dic_row, dic_notes):
#    #florale
#    if int(dic_row["dgav_geo_affect"][:3]) == 556:
#        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 6, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
#        return dic_notes
#    #tournante
#    elif int(dic_row["dgav_geo_affect"][:3]) == 557:
#        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 6, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
#        return dic_notes

def not_niv_1_ED(dic_row, dic_notes):
    ####VEGETATION
    ED_vegetation = []
    ED_vegetation.append("Friches contrôlées")
    ED_vegetation.append("Zones boisées sur herbe")
    ED_vegetation.append("Haies taillées")
    ED_vegetation.append("Massifs d'ornement")
    ED_vegetation.append("Arbustes et couvre-sols indigènes")
    ED_vegetation.append("Arbustes et couvre-sols d'ornement")
    ED_vegetation.append("Gazons extensifs")
    ED_vegetation.append("Gazons intensifs")
    ED_vegetation.append("Plantages")
    ED_vegetation.append("Prairies")
    ED_vegetation.append("Zones boisées")
    ED_vegetation.append("Zones viticoles - agricoles")
    
    ####AUTRE
    ED_autre = []
    ED_autre.append("Enrochements")
    ED_autre.append("Tombes")    
    
    ####REVETEMENTS SEMI PERMEABLES
    ED_rev_semi_perm = []
    ED_rev_semi_perm.append("Revêtements meubles")
    ED_rev_semi_perm.append("Revêtements synthétiques")
    ED_rev_semi_perm.append("Revêtements perméables")    
    
    ####EAUX
    ED_eaux = []
    ED_eaux.append("Bassins / fontaines")
    ED_eaux.append("Plans d'eau naturels et rivières")
    
    if dic_row["ed_inventaire_cat_entret"] in ED_vegetation:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 2, "prof_sol" : 4, "prof_sol_fiab" : 2, "porosite" : 4, "porosite_fiab" : 2, "mo_argile" : 4, "mo_argile_fiab" : 2 }
        return not_niv_2_ED_veget(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] in ED_eaux:
        dic_notes = {"perm_surf" : 1, "perm_surf_fiab" : 6, "prof_sol" : 1, "prof_sol_fiab" : 6, "porosite" : 1, "porosite_fiab" : 6, "mo_argile" : 1, "mo_argile_fiab" : 6 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] in ED_autre:
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 2, "prof_sol" : 2, "prof_sol_fiab" : 2, "porosite" : 2, "porosite_fiab" : 2, "mo_argile" : 2, "mo_argile_fiab" : 2 }
        return not_niv_2_ED_autre(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] in ED_rev_semi_perm:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 2, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 2, "mo_argile_fiab" : 4 }
        #return not_niv_2_ED_semi_perm(dic_row, dic_notes)
        return dic_notes
    else:
        #il ne devrait pas y en avoir
        dic_notes = {"perm_surf" : 20, "perm_surf_fiab" : 20, "prof_sol" : 20, "prof_sol_fiab" : 20, "porosite" : 20, "porosite_fiab" : 20, "mo_argile" : 20, "mo_argile_fiab" : 20 }
        return dic_notes
    
def not_niv_2_ED_veget(dic_row, dic_notes):
    if dic_row["ed_inventaire_cat_entret"] == "Friches contrôlées":
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 5, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 2, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Plantages":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Prairies":
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Zones viticoles - agricoles":
        dic_notes = {"perm_surf" : 3.5, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
        return not_niv_3_ED_z_vit_agr(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] == "Zones boisées":
        dic_notes = {"perm_surf" : 5.7, "perm_surf_fiab" : 4, "prof_sol" : 5.3, "prof_sol_fiab" : 4, "porosite" : 5.3, "porosite_fiab" : 4, "mo_argile" : 5.3, "mo_argile_fiab" : 4 }
        return not_niv_3_ED_z_bois(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] in ["Zones boisées sur herbe", "Haies taillées", "Massifs d'ornement", "Arbustes et couvre-sols indigènes", "Arbustes et couvre-sols d'ornement" ]:
        dic_notes = {"perm_surf" : 4.5, "perm_surf_fiab" : 3, "prof_sol" : 4.5, "prof_sol_fiab" : 3, "porosite" : 4.9, "porosite_fiab" : 3, "mo_argile" : 4.9, "mo_argile_fiab" : 3 }
        return not_niv_3_ED_z_mix(dic_row, dic_notes)
    elif dic_row["ed_inventaire_cat_entret"] in ["Gazons extensifs", "Gazons intensifs"]:
        dic_notes = {"perm_surf" : 3.5, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3.5, "porosite_fiab" : 3, "mo_argile" : 3.5, "mo_argile_fiab" : 3 }
        return not_niv_3_ED_gaz(dic_row, dic_notes)
    else:
        dic_notes = {"perm_surf" : 25, "perm_surf_fiab" : 25, "prof_sol" : 25, "prof_sol_fiab" : 25, "porosite" : 25, "porosite_fiab" : 25, "mo_argile" : 25, "mo_argile_fiab" : 25 }
        return dic_notes
    
'''IL MANQUE LES NOTES DU NIVEAU 2 ! NA DANS LE FICHIER EXCEL'''
def not_niv_2_ED_autre(dic_row, dic_notes):
    if dic_row["ed_inventaire_cat_entret"] == "Enrochements":
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Tombes":
        return dic_notes
    else :
        dic_notes = {"perm_surf" : 26, "perm_surf_fiab" : 26, "prof_sol" : 26, "prof_sol_fiab" : 26, "porosite" : 26, "porosite_fiab" : 26, "mo_argile" : 26, "mo_argile_fiab" : 26 }
        return dic_notes

#'''IL MANQUE LES NOTES DU NIVEAU 2 ! NA DANS LE FICHIER EXCEL'''
#def not_niv_2_ED_semi_perm(dic_row, dic_notes):
#    if dic_row["ed_inventaire_cat_entret"] == "Revêtements meubles":
#        #dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
#        return dic_notes
#    elif dic_row["ed_inventaire_cat_entret"] == "Revêtements synthétiques":
#        #dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
#        return dic_notes
#    elif dic_row["ed_inventaire_cat_entret"] == "Revêtements perméables":
#        #dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
#        return dic_notes
#    else :
#        #dic_notes = {"perm_surf" : 27, "perm_surf_fiab" : 27, "prof_sol" : 27, "prof_sol_fiab" : 27, "porosite" : 27, "porosite_fiab" : 27, "mo_argile" : 27, "mo_argile_fiab" : 27 }
#        return dic_notes

def not_niv_3_ED_z_mix(dic_row, dic_notes):
    if dic_row["ed_inventaire_cat_entret"] == "Zones boisées sur herbe":
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 4, "porosite" : 5.5, "porosite_fiab" : 4, "mo_argile" : 5.5, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Haies taillées":
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Massifs d'ornement":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] in ["Arbustes et couvre-sols indigènes", "Arbustes et couvre-sols d'ornement" ]:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_ED_gaz(dic_row, dic_notes):
    if dic_row["ed_inventaire_cat_entret"] == "Gazons extensifs":
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_cat_entret"] == "Gazons intensifs":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes  

def not_niv_3_ED_z_bois(dic_row, dic_notes):
    if dic_row["ed_inventaire_entretien"] == "Aire forestière":
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 6, "prof_sol_fiab" : 4, "porosite" : 6, "porosite_fiab" : 5, "mo_argile" : 6, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["ed_inventaire_entretien"] == "Bosquet":
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["ed_inventaire_entretien"] in ["Cordon boisé", "Cordon Boisé"]:
        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 4, "prof_sol" : 5, "prof_sol_fiab" : 4, "porosite" : 5, "porosite_fiab" : 4, "mo_argile" : 5, "mo_argile_fiab" : 4 }
        return dic_notes
    else:
        #il ne devrait pas y en avoir
        dic_notes = {"perm_surf" : 28, "perm_surf_fiab" : 28, "prof_sol" : 28, "prof_sol_fiab" : 28, "porosite" : 28, "porosite_fiab" : 28, "mo_argile" : 28, "mo_argile_fiab" : 28 }
        return dic_notes

def not_niv_3_ED_z_vit_agr(dic_row, dic_notes):
    if dic_row["ed_inventaire_entretien"] in ["Vignes", "Zone viticole"]:
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["ed_inventaire_entretien"] == "Zone agricole":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
        return dic_notes
    else:
        #il ne devrait pas y en avoir
        dic_notes = {"perm_surf" : 29, "perm_surf_fiab" : 29, "prof_sol" : 29, "prof_sol_fiab" : 29, "porosite" : 29, "porosite_fiab" : 29, "mo_argile" : 29, "mo_argile_fiab" : 29 }
        return dic_notes

def not_niv_1_aff(dic_row, dic_notes):
    #### NOTATION EN ZONE a batir OU PAS
    #liste de affectations considerees comme zab
    affectations_en_zab = []
    affectations_en_zab.append("Zone d'activités artisanales")
    affectations_en_zab.append("Zone d'activités tertiaires")
    affectations_en_zab.append("Zone de camping")
    affectations_en_zab.append("Zone de centre de localité (zone village)")
    affectations_en_zab.append("Zone de centre historique")
    affectations_en_zab.append("Zone de hameau")
    affectations_en_zab.append("Zone de site construit protégé")
    affectations_en_zab.append("Zone d'habitation de très faible densité")
    affectations_en_zab.append("Zone d'habitation de faible densité")
    affectations_en_zab.append("Zone d'habitation de moyenne densité")
    affectations_en_zab.append("Zone d'habitation de forte densité")
    affectations_en_zab.append("Zone de sport et loisirs")
    affectations_en_zab.append("Zone de verdure")
    affectations_en_zab.append("Zone d'installations (para-) publiques")
    affectations_en_zab.append("Zone industrielle")
    affectations_en_zab.append("Zone à options")
    
    #liste de affectations considerees hors zab
    affectations_hors_zab = []
    affectations_hors_zab.append("Aire forestière")
    affectations_hors_zab.append("Zone d'extraction et dépôt de matériaux")
    affectations_hors_zab.append("Zone intermédiaire")
    affectations_hors_zab.append("Zone naturelle protégée")
    affectations_hors_zab.append("Zone agricole")
    affectations_hors_zab.append("Zone agricole protégée")
    affectations_hors_zab.append("Zone agricole spécialisée")
    affectations_hors_zab.append("Zone para-agricole")
    affectations_hors_zab.append("Zone viticole")
    affectations_hors_zab.append("Zone viticole protégée")
    
    #notation du domaine public
    if dic_row["paf_type_princ"] == "DP":
        dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 2.5, "prof_sol_fiab" : 1, "porosite" : 3, "porosite_fiab" : 1, "mo_argile" : 3, "mo_argile_fiab" : 1 }
        return dic_notes
        
    #elif dic_row["paf_type_princ"] == "Zone ferroviaire":
    #    dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 2.5, "prof_sol_fiab" : 1, "porosite" : 2.5, "porosite_fiab" : 1, "mo_argile" : 2.5, "mo_argile_fiab" : 1 }
    #    return dic_notes
    
    #notation de la zab
    elif dic_row["paf_type_princ"] in affectations_en_zab:
        dic_notes = {"perm_surf" : 2.5, "perm_surf_fiab" : 1, "prof_sol" : 2.5, "prof_sol_fiab" : 1, "porosite" : 2.5, "porosite_fiab" : 1, "mo_argile" : 2.5, "mo_argile_fiab" : 1 }
        #print(dic_notes["perm_surf"])
        return not_niv_2_aff_zab_v_pas_v(dic_row, dic_notes)
    
    #notation hors zab
    elif dic_row["paf_type_princ"] in affectations_hors_zab:
        #dic_notes = {"perm_surf" : 17, "perm_surf_fiab" : 17, "prof_sol" : 17, "prof_sol_fiab" : 17, "porosite" : 17, "porosite_fiab" : 17, "mo_argile" : 17, "mo_argile_fiab" : 17 }
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 1, "prof_sol" : 4, "prof_sol_fiab" : 1, "porosite" : 3, "porosite_fiab" : 1, "mo_argile" : 3, "mo_argile_fiab" : 1 }
        return not_niv_2_aff_hors_zab(dic_row, dic_notes)
    else :
        return dic_notes

def not_niv_2_aff_hors_zab(dic_row, dic_notes):
    if dic_row["paf_type_princ"] == "Aire forestière" or (dic_row["paf_type_princ"] == "Zone naturelle protégée" and dic_row["paf_type_sec"] == "Aire forestière"):
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 6, "prof_sol_fiab" : 4, "porosite" : 6, "porosite_fiab" : 5, "mo_argile" : 6, "mo_argile_fiab" : 5 }
        return not_niv_3_aff_hors_zab_foret(dic_row, dic_notes)
    elif dic_row["paf_type_princ"] == "Zone d'extraction et dépôt de matériaux":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 5, "prof_sol" : 2, "prof_sol_fiab" : 5, "porosite" : 2, "porosite_fiab" : 5, "mo_argile" : 2, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone intermédiaire":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        #return not_niv_3_aff_hors_zab_inter(dic_row, dic_notes)
        return dic_notes
    elif dic_row["paf_type_princ"] in ["Zone agricole", "Zone agricole protégée", "Zone naturelle protégée", "Zone agricole spécialisée", "Zone para-agricole", "Zone viticole", "Zone viticole protégée"]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_3_aff_hors_zab_agr(dic_row, dic_notes)
    else:
        '''il ne devrait pas y en avoir...'''
        dic_notes = {"perm_surf" : 22, "perm_surf_fiab" : 22, "prof_sol" : 22, "prof_sol_fiab" : 22, "porosite" : 22, "porosite_fiab" : 22, "mo_argile" : 22, "mo_argile_fiab" : 22 }
        return dic_notes

def not_niv_3_aff_hors_zab_foret(dic_row, dic_notes):
    #il y a de l'aire forestière en zone protégée et des zones protégées en aire forestiere
    if (dic_row["paf_type_princ"] == "Aire forestière" and dic_row["paf_type_sec"] == "Zone naturelle protégée") or (dic_row["paf_type_princ"] == "Zone naturelle protégée" and dic_row["paf_type_sec"] == "Aire forestière"):
        dic_notes = {"perm_surf" : 6,  "perm_surf_fiab" : 6, "prof_sol" : 6, "prof_sol_fiab" : 4, "porosite" : 6, "porosite_fiab" : 6, "mo_argile" : 6, "mo_argile_fiab" : 6 }
        #return not_niv_4_aff_hors_zab_foret_nat(dic_row, dic_notes)
        return dic_notes
    else: #= autre
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 4, "prof_sol" : 6, "prof_sol_fiab" : 4, "porosite" : 6, "porosite_fiab" : 5, "mo_argile" : 6, "mo_argile_fiab" : 5 }
        return dic_notes

#def not_niv_3_aff_hors_zab_inter(dic_row, dic_notes):
#    if dic_row["paf_denom_com"] == "Zone de réserve":
#        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 1, "prof_sol" : 3, "prof_sol_fiab" : 1, "porosite" : 3, "porosite_fiab" : 1, "mo_argile" : 3, "mo_argile_fiab" : 1 }
#        return dic_notes
#    elif dic_row["paf_denom_com"] == "Zone verte du bas du village":
#        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 1, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 3, "mo_argile_fiab" : 1 }
#        return dic_notes
#    else: #=autre
#        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 1, "prof_sol" : 3, "prof_sol_fiab" : 1, "porosite" : 3, "porosite_fiab" : 1, "mo_argile" : 3, "mo_argile_fiab" : 1 }
#        return dic_notes

def not_niv_3_aff_hors_zab_agr(dic_row, dic_notes):
    if dic_row["paf_type_princ"] in ["Zone agricole protégée", "Zone viticole protégée"]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return not_niv_4_aff_hors_zab_agr_pro(dic_row, dic_notes)
    elif dic_row["paf_type_sec"] == "Zone naturelle protégée" or (dic_row["paf_type_princ"] == "Zone naturelle protégée" and dic_row["paf_type_sec"] == "Zone agricole protégée"):
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 5, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 4, "mo_argile_fiab" : 5 }
        return not_niv_4_aff_hors_zab_agr_nat(dic_row, dic_notes)
    elif dic_row["paf_type_princ"] == "Zone agricole spécialisée":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 4, "mo_argile_fiab" : 4 }
        #return not_niv_4_aff_hors_zab_agr_spe(dic_row, dic_notes)
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone para-agricole":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 4, "prof_sol" : 2, "prof_sol_fiab" : 4, "porosite" : 2, "porosite_fiab" : 4, "mo_argile" : 2, "mo_argile_fiab" : 4 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone viticole" or dic_row["paf_type_sec"] == "Zone viticole":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else: #autre, i.e. type_princ == "Zone agricole"
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes

#def not_niv_4_aff_hors_zab_foret_nat(dic_row, dic_notes):
#    if dic_row["paf_denom_com"] == "Zone alluviale d'importance nationale":
#        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 6, "prof_sol" : 6, "prof_sol_fiab" : 6, "porosite" : 6, "porosite_fiab" : 6, "mo_argile" : 6, "mo_argile_fiab" : 6 }
#        return dic_notes
#    elif dic_row["paf_denom_com"] == "Aire forestière, réserve naturelle Le Bomelet":
#        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 6, "prof_sol" : 6, "prof_sol_fiab" : 6, "porosite" : 6, "porosite_fiab" : 6, "mo_argile" : 6, "mo_argile_fiab" : 6 }
#        return dic_notes
#    elif dic_row["paf_denom_com"] in ["Zone protégées des couloirs de la Venoge et du Veyron", "Zone protégée des couloirs de la Venoge et du Veyron", "Zone de protection des couloirs de la Venoge et du Veyron"]:
#        dic_notes = {"perm_surf" : 6, "perm_surf_fiab" : 6, "prof_sol" : 6, "prof_sol_fiab" : 6, "porosite" : 6, "porosite_fiab" : 6, "mo_argile" : 6, "mo_argile_fiab" : 6 }
#        return dic_notes
#    else:
#        return dic_notes
        
def not_niv_4_aff_hors_zab_agr_pro(dic_row, dic_notes):
    if dic_row["paf_type_princ"] == "Zone viticole protégée" or dic_row["paf_type_sec"] == "Zone viticole protégée":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes
    else:#=autre
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 4, "prof_sol" : 3, "prof_sol_fiab" : 4, "porosite" : 3, "porosite_fiab" : 4, "mo_argile" : 3, "mo_argile_fiab" : 4 }
        return dic_notes

def not_niv_4_aff_hors_zab_agr_nat(dic_row, dic_notes):
    if dic_row["paf_type_sec"] == "Zone alluviale d'importance nationale":
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 5, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 4, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["paf_denom_com"] == "Zone agricole, réserve naturelle Le Bomelet":
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 5, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 4, "mo_argile_fiab" : 5 }
        return dic_notes
    elif dic_row["paf_denom_com"] in ["Zone protégées des couloirs de la Venoge et du Veyron", "Zone protégée des couloirs de la Venoge et du Veyron", "Zone de protection des couloirs de la Venoge et du Veyron"]:
        dic_notes = {"perm_surf" : 5, "perm_surf_fiab" : 5, "prof_sol" : 4, "prof_sol_fiab" : 4, "porosite" : 4, "porosite_fiab" : 5, "mo_argile" : 4, "mo_argile_fiab" : 5 }
        return dic_notes
    else:
        return dic_notes

#def not_niv_4_aff_hors_zab_agr_spe(dic_row, dic_notes):
#    if dic_row["paf_denom_com"] == "Zone maraîchere et/ou horticole":
#        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 3, "mo_argile" : 3, "mo_argile_fiab" : 3 }
#        return dic_notes
#    else:#= autre
#        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 3, "mo_argile" : 3, "mo_argile_fiab" : 3 }
#        return dic_notes

def not_niv_2_aff_zab_v_pas_v(dic_row, dic_notes):
    #si les polygones ont une info de revetement, ce ne sont pas de batiments
    if dic_row["revetement"] == "vert":
        dic_notes = {"perm_surf" : 2.9, "perm_surf_fiab" : 2, "prof_sol" : 3.0, "prof_sol_fiab" : 2, "porosite" : 2.9, "porosite_fiab" : 2, "mo_argile" : 3.0, "mo_argile_fiab" : 2 }
        return not_niv_3_aff(dic_row, dic_notes)
    elif dic_row["revetement"] == "pas_vert":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 2, "prof_sol" : 2, "prof_sol_fiab" : 2, "porosite" : 2, "porosite_fiab" : 2, "mo_argile" : 2, "mo_argile_fiab" : 2 }
        return dic_notes
    else:
        return dic_notes

def not_niv_3_aff(dic_row, dic_notes):
    '''attention a bien prendre la denomination de la base de donnees qui peut etre differente de celle des fichiers excel'''
    if dic_row["paf_type_princ"] == "Zone d'activités artisanales":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone d'activités tertiaires":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de camping":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 3, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de centre de localité (zone village)":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de centre historique":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de hameau":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de site construit protégé":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] in ["Zone d'habitation de très faible densité", "Zone d'habitation de faible densité", "Zone d'habitation de moyenne densité", "Zone d'habitation de forte densité"]:
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de sport et loisirs":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 3, "prof_sol" : 3, "prof_sol_fiab" : 3, "porosite" : 3, "porosite_fiab" : 3, "mo_argile" : 3, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone de verdure":
        dic_notes = {"perm_surf" : 4, "perm_surf_fiab" : 3, "prof_sol" : 4, "prof_sol_fiab" : 3, "porosite" : 4, "porosite_fiab" : 3, "mo_argile" : 4, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone d'installations (para-) publiques":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone industrielle":
        dic_notes = {"perm_surf" : 2, "perm_surf_fiab" : 3, "prof_sol" : 2, "prof_sol_fiab" : 3, "porosite" : 2, "porosite_fiab" : 3, "mo_argile" : 2, "mo_argile_fiab" : 3 }
        return dic_notes
    elif dic_row["paf_type_princ"] == "Zone à options":
        dic_notes = {"perm_surf" : 3, "perm_surf_fiab" : 2, "prof_sol" : 3, "prof_sol_fiab" : 2, "porosite" : 3, "porosite_fiab" : 2, "mo_argile" : 3, "mo_argile_fiab" : 2 }
        return dic_notes
    else:
        #il ne devrait pas y en avoir
        dic_notes = {"perm_surf" : 10, "perm_surf_fiab" : 10, "prof_sol" : 10, "prof_sol_fiab" : 10, "porosite" : 10, "porosite_fiab" : 10, "mo_argile" : 10, "mo_argile_fiab" : 10 }
        return dic_notes

create_folder(temp_folder)
database_not = copy_vector_DB(database_in)
conn = create_connection(database_not)
processing(conn)
delete_folder(temp_folder)


