# Ce script prend en entree le sqlite de la fusion des BDs avec fme
# Il decoupe l'image IR selon les polygones choisis (batiments hs et sout)
# Il calcule le ndvi moyen pour chaque polygone
# Il ajoute un champ ndvi avec sa valeur dans la BD
# La BD en sortie a le suffixe bat_ndvi.sqlite

from subprocess import call
import sqlite3
import json
import shapely
from shapely import wkt
import geodaisy.converters as convert
import gdal
import numpy as np
import os
import shutil

def create_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    try:
        os.mkdir(folder)
    except OSError:
        print("Creation of the folder %s failed " % folder)

def copy_db(folder, file_name):
    database = os.path.join(folder, file_name)
    database_mod = f"{database[:-7]}_bat_ndvi.sqlite"
    if os.path.exists(database_mod):
        os.remove(database_mod)
    shutil.copyfile(database, database_mod)
    return database_mod

def crop_raster(folder, raster_rs_in, path_to_bats, json, name):
    srcraster = os.path.join(folder, raster_rs_in)
    name_out = f"{name}_crop.tif"
    dstfile = os.path.join(path_to_bats, name_out)
    #certains polygones sont non valides et le cutline ne fonctionne pas...
    commande_crop_raster_with_polygon = f"gdalwarp -s_srs EPSG:2056 -t_srs EPSG:2056 -cutline {json} -crop_to_cutline -overwrite -dstnodata -9999 {srcraster} {dstfile}"
    call(commande_crop_raster_with_polygon)
    return dstfile

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        conn.enable_load_extension(True)
        conn.execute('SELECT load_extension("mod_spatialite")') #P*****... c'est la ligne qu'il manquait pour décoder la geometrie :-\
    except sqlite3.Error as e:
        print(e)
    return conn

def add_column_DB_float(conn, table, nom_col):
    cur = conn.cursor()
    try :
        addColumn = f"ALTER TABLE {table} ADD COLUMN {nom_col} FLOAT"
        cur.execute(addColumn)
        conn.commit()
    except sqlite3.Error as e:
        print(e)

def select_buildings(conn):
    cur = conn.cursor()
    request= f"SELECT astext(geometry), OGC_FID, baths_s_design, batss_s_design FROM calc_vectoriel"
    cur.execute(request)
    rows = cur.fetchall()
    return rows

def calcul_ndvi(path_OTBcli, raster_cut):
    NDVI = raster_cut[:-4] + "_ndvi.tif"
    commande_NDVI = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 32000 -il {raster_cut} -out {NDVI} -exp "(im1b1-im1b2)/(im1b1+im1b2)"'
    call(commande_NDVI)
    return NDVI

##calcule la moyenne du ndvi et ecrit cette valeur dans la base de donnees
def get_mean_ndvi(ndvi, conn, ogcfid):
    ndvi_mean = ndvi[:-4] + "_moy.tif"
    raster = gdal.Open(ndvi)
    bands = raster.RasterCount
    data = raster.GetRasterBand(1).ReadAsArray().astype('float')
    mean_data = np.mean(data[data<=1])#pour exclure les nodata qui sont "nan"
    sql = '''UPDATE calc_vectoriel SET ndvi=? WHERE OGC_FID = ?'''
    cur = conn.cursor()
    values = (round(mean_data,5), ogcfid)
    cur.execute(sql, values)
    conn.commit()
    return ndvi_mean

def process_buildings(folder_interm, rows, conn, folder_out, raster_rs_in, path_OTBcli):
    number = 0
    for row in rows:
        wkt_geometry = row[0]
        ogcfid = row[1]
        building_type = row[2]
        ug_building_type = row[3]
        geom = shapely.wkt.loads(wkt_geometry)
        if building_type not in ["None", None, ""] or ug_building_type not in ["None", None, ""]: 
            print("building_type: ", building_type)
            json = convert.wkt_to_geojson(str(geom))
            json_mn95 = '{"type": "FeatureCollection", "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::2056" } }, "features": [{ "type": "Feature", "geometry":' + json + '}]}'
            name_json_file = f"{number}_json_file.geojson"
            json_file = os.path.join(folder_interm, name_json_file)
            with open(json_file, "w") as js_f:
                js_f.write(json_mn95)
            name = str(number) + "_bat"
            cut = crop_raster(folder_out, raster_rs_in, folder_interm, json_file, name)
            ndvi = calcul_ndvi(path_OTBcli, cut)
            try:
                ndvi_mean = get_mean_ndvi(ndvi, conn, ogcfid)
            except AttributeError:
                print("ce batiment ne peux pas etre traite. Geometrie probablement invalide")
            number += 1
        else:
            pass

#removing of temp folder
def delete_folder(path_to_folder):
    try :
        shutil.rmtree(path_to_folder)
        print("temp folder removed")
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (path_to_folder, e)) 
            
def main():
    path_OTBcli = r".\..\SOFTS\OTB-7.1.0-Win64\bin"
    folder_interm = r".\..\DATA_inter"
    folder_out = r".\..\DATA_out"

    raster_rs_in = "mosaic_RS_ui16_norm.tif"
    database_in = "fusion_bd_vectoriel.sqlite"
    
    create_folder(folder_interm)
    database_mod = copy_db(folder_out, database_in)

    table = "calc_vectoriel"
 
    #nom de la colonne a ajouter
    new_col_ndvi = "ndvi"

    #connexion a la base de donnees issue du traitement vectoriel
    conn = create_connection(database_mod)
    with conn:
        add_column_DB_float(conn, table, new_col_ndvi)
        rows = select_buildings(conn)
        process_buildings(folder_interm, rows, conn, folder_out, raster_rs_in, path_OTBcli)
    
    delete_folder(folder_interm)
    
if __name__ == '__main__':
    main()
