#ce script prend en entree les bandes swiss image RS non rectifiees ni egalisees, parfois superposees
#il nettoie les bords des images (suppression des fausses valeurs)
#il assemble les tuiles tout en effectuant une egalisation radiometrique
#il normalise les bandes entre 0 et 255 (vals par defaut de otbcli_DynamicConvert)

import os
from subprocess import call
import shutil

path_OTBcli = r".\..\SOFTS\OTB-7.1.0-Win64\bin"
path_data_in = r".\..\DATA_in"
path_data_inter = r".\..\DATA_inter"
path_data_out = r".\..\DATA_out"
folder_img = "SwissimageRS"

#creation of temp folder
def create_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    try:
        os.mkdir(folder)
    except OSError:
        print("Creation of the folder %s failed " % folder)

def cleaning_swissimageRS(path_OTBcli, path_data_in, folder_img, path_data_inter):
    images_path = os.path.join(path_data_in, folder_img)
    for element in os.listdir(images_path):
        if element.endswith(".tif"):
            image_RS_in = os.path.join(images_path, element)
            image_RS_out = os.path.join(path_data_inter, element)[:-4]+"_mod.tif"
            clean_nodata = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 60000 -il {image_RS_in} -out {image_RS_out} uint16 -exp "(vmin(im1b1N5x5)[0]>0)? im1:{{0,0,0,0}}"' #le nombre doit etre impair min(im1b1,im1b2,im1b3,im1b4)==1 ? im1 : 0
            call(clean_nodata)
    
def mosaic_images(path_OTBcli, path_data_inter):
    mosaic_RS_ui16 = os.path.join(path_data_inter, "mosaic_RS_ui16.tif")
    list_img_RS_mod=[]
    for image_mod in os.listdir(path_data_inter):
        if image_mod.endswith("_mod.tif"):
            list_img_RS_mod.append(os.path.join(path_data_inter, image_mod))
    command_SIRS = f'{path_OTBcli}\otbcli_Mosaic.bat -ram 50000 -il {" ".join(list_img_RS_mod)} -output.spacingx 0.1 -output.spacingy 0.1 -comp.feather large -out {mosaic_RS_ui16} uint16'
    call(command_SIRS)
    return mosaic_RS_ui16
    
def normalisation(path_OTBcli, mosaic_RS_ui16, path_data_out): #arg : nom de l'image
    mosaic_norm = os.path.join(path_data_out, "mosaic_RS_ui16_norm.tif")
    commande_norm_RS = f'{path_OTBcli}\otbcli_DynamicConvert.bat -in {mosaic_RS_ui16} -ram=5000 -out {mosaic_norm} uint16'
    call(commande_norm_RS)

#removing of temp folder
def delete_folder(path_to_folder):
    try :
        shutil.rmtree(path_to_folder)
        print("temp folder removed")
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (path_to_folder, e))

create_folder(path_data_inter)
create_folder(path_data_out)
cleaning_swissimageRS(path_OTBcli, path_data_in, folder_img, path_data_inter)
mosaic_RS_ui16 = mosaic_images(path_OTBcli, path_data_inter)
normalisation(path_OTBcli, mosaic_RS_ui16, path_data_out)
delete_folder(path_data_inter)
