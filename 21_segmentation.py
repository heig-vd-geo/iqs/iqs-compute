#IQS Morges - script non utilise dans la version finale
#ce script prepare les donnees necessaires a la segmentation, pour une zone d'emprise donnee.
#il prend en entree : TPI_norm.tif, TWI_norm.tif, MNH_norm.tif, mosaic_RGB.vrt, mosaic_RS_ui16_norm.tif (qui se trouvent dans le dossier path_data_out / generes par traitement_donnees.py)
#il accumule ensuite les differents rasters dans un seul raster
#il calcule enfin la segmentation de ce raster et genere un fichier vectoriel
#apres la segmentation, on peut decouper avec les batiments et routes de la mens off (clip_mo -> fme)

import os
from subprocess import call

#CHOISIR UNE DES ZONES DE TRAVAIL CI DESSOUS

#nom_zone = "Lonay_Echandens"
#emprise_UL_LR = "2529775 1154330 2531775 1153065"

#nom_zone = "Morges_Lonay"
#emprise_UL_LR = "2527875 1153100 2530045 1151810"

nom_zone = "Morges_Tolochenaz"
emprise_UL_LR = "2526155 1151540 2528145 1150325"

path_data_out = r"D:\Temp_Bron\08_IQS_Morges\DATA_OUT"
path_data_zones_test = r"D:\Temp_Bron\08_IQS_Morges\zones_tests"
path_data_zone = os.path.join(path_data_zones_test, nom_zone)
path_OTBcli = r"C:\Users\maximin.bron\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\OTB-7.1.0-Win64\bin"

#Decoupe des raster a l'emprise de la zone de travail
def crop_a_l_emprise(path_data_out, path_data_zone, emprise_UL_LR):
    images_to_crop=("TPI_norm.tif", "TWI_norm.tif", "MNH_norm.tif" , "mosaic_RGB.vrt", "mosaic_RS_ui16_norm.tif")
    for image_to_crop in os.listdir(path_data_out):
        if image_to_crop.endswith(images_to_crop):
            image_in = os.path.join(path_data_out, image_to_crop)
            image_cropped = os.path.join(path_data_zone, f"{image_to_crop[:-4]}_crop.tif")
            command_crop = f"gdal_translate -projwin {emprise_UL_LR} {image_in} {image_cropped}"
            os.system(command_crop)
    return image_cropped

#surechantillonage des raster pour qu'ils aient tous une resolution de 0.1m
def surechantillonage(path_data_zone, resolution):
    images_to_subs = ("TPI_norm_crop.tif", "TWI_norm_crop.tif", "MNH_norm_crop.tif")
    for image_to_subs in os.listdir(path_data_zone):
        if image_to_subs.endswith(images_to_subs):
            image_in = os.path.join(path_data_zone, image_to_subs)
            image_subs = os.path.join(path_data_zone, f"{image_to_subs[:-4]}_subs.tif")
            command_subs = f"gdal_translate {image_in} {image_subs} -r bilinear -tr {resolution} {resolution}"
            os.system(command_subs)
    return image_subs

#Fusion des couches dans un meme raster pour pouvoir ensuite calculer la segmentation
def superposition_couches(path_data_zone):
    RGB_TPI_TWI_MNH = os.path.join(path_data_zone, "RGB_TPI_TWI_MNH.tif")
    images_to_merge = ("mosaic_RGB_crop.tif", "TPI_norm_crop_subs.tif", "TWI_norm_crop_subs.tif", "MNH_norm_crop_subs.tif")
    to_merge = ""
    for image in os.listdir(path_data_zone):
        if image.endswith(images_to_merge):
            to_merge += f"{os.path.join(path_data_zone, image)} "
    merge_RGB_TPI_TWI_MNH = f"gdal_merge.py -o {RGB_TPI_TWI_MNH} {to_merge} -separate"
    print(merge_RGB_TPI_TWI_MNH)
    os.system(merge_RGB_TPI_TWI_MNH)
    return RGB_TPI_TWI_MNH

#path_data_to_segment = "D:\\Temp_Bron\\08_IQS_Morges\\zones_tests\\Morges_Tolochenaz"
#nom_du_raster_a_segmenter = "RGB_TPI_TWI_MNH.tif"
#os.chdir(path_data_to_segment)

#calcul de la segmentation
def segmentation(raster_a_segmenter, spatialr, ranger, minsize, path_OTBcli) :
    segm_out = f"{raster_a_segmenter[:-4]}_sr{spatialr}_rr{ranger}_ms{minsize}.sqlite"
    print(segm_out)
    commande_segmentation = f'{path_OTBcli}\otbcli_Segmentation.bat -mode.vector.tilesize null -filter.meanshift.spatialr {spatialr} -filter.meanshift.ranger {ranger} -filter.meanshift.minsize {minsize} -in {raster_a_segmenter} -mode vector -mode.vector.out {segm_out}'
    print(commande_segmentation)
    call(commande_segmentation)

crop_a_l_emprise(path_data_out, path_data_zone, emprise_UL_LR)
surechantillonage(path_data_zone, 0.1)
raster_accum = superposition_couches(path_data_zone)
segmentation(raster_accum, 10, 30, 5000, path_OTBcli)
