#this function creates a vrt file (virtual file, reading source images to call them from a single file)
#images must be kept at the same place otherwise the vrt file has to be regenerated
#USE OSGEO4W Shell to execute it (translation might fail in Windows command line)

import os
from subprocess import call
import xml.etree.ElementTree as ET

folder_images_RGB = r".\..\DATA_in\SwissimageRGB"
mosaic_out = r".\..\DATA_out\mosaic_RGB.vrt"

def generate_vrtfile(folder_images_RGB, mosaic_out):
    input_gdal = ""
    for image in os.listdir(folder_images_RGB):
        if image.endswith("tif"):
            input_gdal += os.path.join(folder_images_RGB, image) + " "
    commande_gdal = f"gdalbuildvrt {mosaic_out} {input_gdal} -a_srs EPSG:2056"
    call(commande_gdal)

def set_relative_path(mosaic_out):
    tree = ET.parse(mosaic_out)
    root = tree.getroot()
    for attrib in root.iter('SourceFilename'):
        attrib.set('relativeToVRT', '1')
    tree.write(mosaic_out)

generate_vrtfile(folder_images_RGB, mosaic_out)
set_relative_path(mosaic_out)