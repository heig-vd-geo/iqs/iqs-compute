#Projet IQS Morges / script non utilise dans la version finale

#Ce script prend en entree le nuage du LiDAR fusionne au prealable (-> tous_nuages.las) avec Lastools/lasmerge
#Il genere les differents produits normalises (TPI, TWI, MNH)
#ATTENTION, ces produits sont NORMALISES en sortie pour pouvoir ensuite servir de base pour segmentation
#pour recuperer les donnees brutes (non normalisees), commenter la suppresion du dossier DATA_inter...

#Il est impossible de le lancer dans OSGEO4W. La librairie whitebox ne peut pas y etre installee.
#Pour usage avec osgeo4w, modifier le script pour lancer les commandes wbt en command line
#Il peut etre execute d'une seule traite dans le cmd de windows a condition d'y avoir installe gdal (whl de gdal pour python et la lib wbt)

#Source of wbt process :
#https://jblindsay.github.io/wbt_book/tutorials/lidar.html
    

import whitebox
wbt = whitebox.WhiteboxTools()
import os
import numpy as np
from subprocess import call
import rasterio
import xml.etree.ElementTree as ET

current_dir = os.getcwd()
DATA_in = r".\..\DATA_in"
DATA_inter = r".\..\DATA_inter"
DATA_out = r".\..\DATA_out"

path_OTBcli = r"C:\Users\maximin.bron\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins\OTB-7.1.0-Win64\bin"

current_dir = os.getcwd()

#Merge lidar tiles with lastools
####data_rep_LAS = chemin_tuiles_lidar + "LAS"
####commande="lasmerge -i "+ data_rep_LAS + "\\*.las " + "-o " + LAS_tous
####print(commande)
####call(commande)

def create_folder(folder):
    if not os.path.exists(folder):
        try:
            os.mkdir(folder)
        except OSError:
            print("Creation of the folder %s failed " % folder)
    else:
        pass

#this function generates TRI, TPI, TWI and CHM
def compute_from_lidar(current_dir, DATA_in, DATA_inter, path_OTBcli):
    #input data
    LAS_tous = os.path.join(current_dir, DATA_in, "LAS", "tous_nuages.las")
    #intermediate results
    LAS_MNS = os.path.join(current_dir, DATA_inter, "LAS_MNS.las")
    LAS_thin = os.path.join(current_dir, DATA_inter, "LAS_thin.las")
    LAS_ground = os.path.join(current_dir, DATA_inter, "LAS_ground.las")
    MNT_20cm_brut = os.path.join(current_dir, DATA_inter, "MNT_20cm_brut.tif")
    MNT_20cm = os.path.join(current_dir, DATA_inter, "MNT_20cm.tif")
    MNT_10m = os.path.join(current_dir, DATA_inter, "MNT_10m.tif")
    MNS_20cm_brut = os.path.join(current_dir, DATA_inter, "MNS_20cm_brut.tif")
    MNS_20cm = os.path.join(current_dir, DATA_inter, "MNS_20cm.tif")
    slope = os.path.join(current_dir, DATA_inter, "slope.tif")
    TRI = os.path.join(current_dir, DATA_inter, "TRI.tif")
    dem_breached = os.path.join(current_dir, DATA_inter, "dem_breached.tif")
    MNT_filled = os.path.join(current_dir, DATA_inter, "MNT_filled.tif")
    flow_accum = os.path.join(current_dir, DATA_inter, "flow_accum.tif")
    TWI = os.path.join(current_dir, DATA_inter, "TWI.tif")
    TPI_brut = os.path.join(current_dir, DATA_inter, "TPI_brut.tif")
    TPI = os.path.join(current_dir, DATA_inter, "TPI.tif")
    MNT_and_MNS = os.path.join(current_dir, DATA_inter, "MNT_and_MNS.tif")
    MNH = os.path.join(current_dir, DATA_inter, "MNH.tif")
    
    #filter classes to keep only class 2 from lidar(ground)
    wbt.filter_lidar_classes(
        LAS_tous, 
        LAS_ground, 
        exclude_cls='0-1,3-18'
    )
    
    #filter classes to remove unclassified points for DSM
    wbt.filter_lidar_classes(
         LAS_tous, 
         LAS_MNS, 
         exclude_cls='0-1'
    )
    
    #lidar_tin_gridding DTM (10m) to generate indices tpi, twi, etc later
    wbt.lidar_tin_gridding(
        LAS_ground,
        MNT_10m,
        resolution=10,
        max_triangle_edge_length=200 #to avoid holes from large buildings and big triangles. To be replaced by wbt.fill_missing_data ?
    )
    
    #lidar_tin_gridding DTM (0.2m) to generate CHM later
    wbt.lidar_tin_gridding(
        LAS_ground,
        MNT_20cm,
        resolution=0.2,
        max_triangle_edge_length=500
    )
    
    ##fill the holes if needed
    #wbt.fill_missing_data(
    #    MNT_20cm_brut, 
    #    MNT_20cm, 
    #    filter=11
    #)
    
    #cloud filtering, keeping highest points for better DSM
    wbt.lidar_thin(
        LAS_MNS,
        LAS_thin, 
        resolution=0.2, 
        method="highest", 
        save_filtered=False,
    )
    
    #lidar_tin_gridding DSM (0.2m) to generate CHM later
    wbt.lidar_tin_gridding(
        LAS_thin,
        MNS_20cm,
        resolution=0.2,
        max_triangle_edge_length=500
    )
    
    ##fill the holes if needed
    #wbt.fill_missing_data(
    #    MNS_20cm_brut, 
    #    MNS_20cm, 
    #    filter=11
    #)
    
    merge_MNT_MNS = f"gdal_merge -separate -o {MNT_and_MNS} {MNS_20cm} {MNT_20cm}"
    print(merge_MNT_MNS)
    os.system(merge_MNT_MNS)
    
    calcul_MNH = f'gdal_calc -S {MNT_and_MNS} --S_band=1 -T {MNT_and_MNS} --T_band=2 --calc="(S-T)*(S>0)*(T>0)*((S-T)>0)" --outfile={MNH} --NoDataValue=0'#  --overwrite --type=<Float32> --NoDataValue=0"
    print(calcul_MNH)
    os.system(calcul_MNH)
    
    #slope computation
    wbt.slope(
        MNT_10m,
        slope,
        #units='degrees',# not taken into account. Default is %
    )
    
    #generate terrain ruggedness index
    wbt.ruggedness_index(
        MNT_10m,
        TRI,
    )
    
    #wbt.feature_preserving_denoise(MNT_out_10m, MNT_smooth, filter=11, norm_diff=8.0)
    
    wbt.breach_depressions(
        MNT_10m,
        dem_breached,
        max_depth=5.0
    )
    
    wbt.fill_depressions(
        dem_breached,
        MNT_filled
    )
    
    #compute water accumulation
    wbt.d_inf_flow_accumulation(
        MNT_filled, 
        flow_accum, 
        log=True
    )
    
    #compute terrain wettnes index (TWI) from water accumulation and tan(slope)
    raster_1=rasterio.open(flow_accum)
    val_accum=raster_1.read(1)
    raster_2=rasterio.open(slope)
    pente=raster_2.read(1)
    value_TWI = np.log(val_accum/np.tan(pente/360*np.pi))
    profile = raster_1.profile
    with rasterio.open(TWI, 'w', **profile) as dst:
        dst.write(value_TWI, 1)
    
    #generate topographic position index (TPI)
    #90m radius around each pixel (9*10m)
    command_TPI = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 32000 -il {MNT_10m} -out {TPI_brut} -exp "im1-mean(im1b1N9x9)"' #NxN : N has to be an odd number
    call(command_TPI)
    
    #cleaning TPI edges by keeping only values between -20 and 20
    command_clean_TPI = f'{path_OTBcli}\otbcli_BandMathX.bat -ram 32000 -il {TPI_brut} -out {TPI} -exp "im1b1<-20 or im1b1>20 ? 0 : im1b1"'
    call(command_clean_TPI)
    
    return MNH, TRI, TPI, TWI

#this function can be used to convert data to uint16
def float32_to_uint16(DATA_inter):
    path_images = os.path.join(DATA_inter, "a_convertir")
    for image_ft32 in os.listdir(path_images) :
        image_in = os.path.join(path_images, image_ft32)
        image_out = image_in[:-4]+"_ui16.tif"
        conversion = f"gdal_translate {image_in} {image_out} -ot UInt16"
        call(conversion)

#normalisation of results (0->255)
def normalisation(list_to_norm, current_dir, DATA_out, path_OTBcli):
    for raster in list_to_norm:
        print(raster)
        name = f"{os.path.split(raster)[1][:-4]}_norm.tif"
        print(name)
        path_out = os.path.join(current_dir, DATA_out, name)
        print(path_out)
        commande_normalisation = f'{path_OTBcli}\otbcli_DynamicConvert.bat -in {raster} -ram=5000 -out {path_out} float'
        print(commande_normalisation)
        call(commande_normalisation)

#removing of temp folder
def delete_folder(path_to_folder):
    try :
        shutil.rmtree(path_to_folder)
        print("temp folder removed")
    except Exception as e:
        print('Failed to delete %s. Reason: %s' % (path_to_folder, e))

create_folder(DATA_inter)
create_folder(DATA_out)
MNH, TRI, TPI, TWI = compute_from_lidar(current_dir, DATA_in, DATA_inter, path_OTBcli)
##float32_to_uint16(DATA_inter)
normalisation([MNH, TPI, TWI], current_dir, DATA_out, path_OTBcli)
delete_folder(DATA_inter)


